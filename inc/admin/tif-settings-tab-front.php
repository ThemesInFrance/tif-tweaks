<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( 'html' . $count++ , array(
	'type'	=> 'html',
	'value'	=> '<fieldset>'."\n".'<legend>' . esc_html__( 'Tags from WordPress Header', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Disable jQuery', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,remove_jquery', 'checkbox' ),
			'description'	=> esc_html__( 'Be careful, some plugins might not work anymore. Act with caution.', 'tif-tweaks' ),
		),
		$tif_plugin_name . '[tif_callback][remove_jquery]'
	);

	$form->add_input( esc_html__( 'Disable emojis', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,remove_emojis', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][remove_emojis]'
	);

	$form->add_input( esc_html__( 'Disable wp generator', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,wp_generator', 'checkbox' ),
			'description'	=> esc_html__( 'Will remove the version of Wordpress, which can be a security issue.', 'tif-tweaks' ),
		),
		$tif_plugin_name . '[tif_callback][wp_generator]'
	);

	$form->add_input( esc_html__( 'Disable Windows Live Writer', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,wlwmanifest_link', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][wlwmanifest_link]'
	);

	// $form->add_input( esc_html__( 'Disable adjacent links', 'tif-tweaks' ),
	// 	array(
	// 		'type'			=> 'checkbox',
	// 		'value'			=> 1,
	// 		'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,adjacent_links', 'checkbox' ),
	// 	),
	// 	$tif_plugin_name . '[tif_callback][adjacent_links]'
	// );

	$form->add_input( esc_html__( 'Disable short link', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,shortlink', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][shortlink]'
	);

	$form->add_input( esc_html__( 'Deactivate REST-API', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,disable_rest_api', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][disable_rest_api]'
	);

	$form->add_input( esc_html__( 'Disable REST API user endpoints', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,disable_rest_api_users', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][disable_rest_api_users]'
	);

$form->add_input( 'html' . $count++, array(
	'type'	=> 'html',
	'value'	=> '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Comments', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Close comments', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_callback,close_comments', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_callback,close_comments', 'multicheck' ),
			'options'		=> array(
				'post'			=> esc_html__( 'Posts', 'tif-tweaks' ),
				'page'			=> esc_html__( 'Pages', 'tif-tweaks' ),
				'attachment'	=> esc_html__( 'Attachements', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_callback][close_comments]'
	);

	$form->add_input( esc_html__( 'Remove css classes from comments', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_callback,remove_comments_class', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_callback,remove_comments_class', 'multicheck' ),
			'options'		=> array(
				'byuser'		=> esc_html__( '"byuser" class', 'tif-tweaks' ),
				'bypostauthor'	=> esc_html__( '"bypostauthor" class', 'tif-tweaks' ),
				'commentauthor'	=> esc_html__( '"comment-author-" class (recommended)', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_callback][remove_comments_class]'
	);

	$form->add_input( esc_html__( 'Move Comment Text Field to Bottom', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,invert_comments_fields', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][invert_comments_fields]'
	);

	$form->add_input( esc_html__( 'Hide url field', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,hide_url_field', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][hide_url_field]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );


$form->add_input( 'html' . $count++ , array(
	'type'	=> 'html',
	'value'	=> '<fieldset>'."\n".'<legend>' . esc_html__( 'RSS Feeds', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Disable main feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,main', 'checkbox' ),
			'description'	=> esc_html__( 'Disabling rss feeds is not desirable to keep the Internet free and open.', 'tif-tweaks' ),
		),
		$tif_plugin_name . '[tif_feeds][main]'
	);

	$form->add_input( esc_html__( 'Disable author page feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,author', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_feeds][author]'
	);

	$form->add_input( esc_html__( 'Disable category page feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,category', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_feeds][category]'
	);

	$form->add_input( esc_html__( 'Disable search page feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,search', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_feeds][search]'
	);

	$form->add_input( esc_html__( 'Disable tag page feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,tag', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_feeds][tag]'
	);

	$form->add_input( esc_html__( 'Disable comments feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,comments', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_feeds][comments]'
	);

	$form->add_input( esc_html__( 'Disable posts comments feed', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_feeds,posts_comments', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_feeds][posts_comments]'
	);

$form->add_input( 'html' . $count++, array(
	'type'	=> 'html',
	'value'	=> '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Authors', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Disable authors pages', 'tif-tweaks' ),
		array(
			'type'			=> 'radio',
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,author_disabled', 'key' ),
			'options'		=> array(
				''				=> esc_html__( 'None', 'canopee' ),
				'unused'		=> esc_html__( 'Author page if their post count is 0 (recommended)', 'canopee' ),
				'all'			=> esc_html__( 'All author pages', 'canopee' ),
			),
		),
		$tif_plugin_name . '[tif_callback][author_disabled]'
	);

	$form->add_input( esc_html__( 'Disabled authors pages status', 'tif-tweaks' ),
		array(
			'type'			=> 'radio',
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,author_status', 'absint' ),
			'options'		=> array(
				404				=> esc_html__( '404', 'canopee' ),
				301				=> esc_html__( '301 redirect', 'canopee' ),
			),
		),
		$tif_plugin_name . '[tif_callback][author_status]'
	);

$form->add_input( 'html' . $count++, array(
	'type'	=> 'html',
	'value'	=> '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'SEO', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Add a noindex tag to pages', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_callback,noindex', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_callback,noindex', 'multicheck' ),
			'options'		=> array(
				'author'			=> esc_html__( 'Author', 'canopee' ),
				'category'			=> esc_html__( 'Categories', 'canopee' ),
				'tag'				=> esc_html__( 'Tags', 'canopee' ),
				'search'			=> esc_html__( 'Search', 'canopee' ),
				'date'				=> esc_html__( 'Archives by dates', 'canopee' ),
			),
			'description'	=> esc_html__( 'It may be advisable to add a noindex tag to some pages to avoid duplicate content. If your site has only one author for example.', 'tif-tweaks' )
		),
		$tif_plugin_name . '[tif_callback][noindex]'
	);

	$form->add_input( esc_html__( 'Missing Canonical', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,missing_canonical', 'checkbox' ),
			'description'	=> esc_html__( 'Add a canonical tag to pages for which Wordpress does not natively display one.', 'tif-tweaks' ),
		),
		$tif_plugin_name . '[tif_callback][missing_canonical]'
	);

	$form->add_input( esc_html__( 'Paginated comments Canonical', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_callback,comments_canonical', 'checkbox' ),
			'description'	=> esc_html__( 'Point back to the original page if comments are paginated to avoid duplicate content issues.', 'tif-tweaks' ),
		),
		$tif_plugin_name . '[tif_callback][comments_canonical]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );
