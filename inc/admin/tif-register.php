<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_reset_theme_mods() {

	// $tif_themes_mods = tif_get_theme_mods_name();
	$tif_themes_mods = array_merge(
		array(
			'header_image',
			'header_image_data',
			'homepage_control',
			'tif_components_version',
		),
		tif_get_theme_mods_name()
	);

	foreach ( $tif_themes_mods as $key ) {

		if ( null != ( get_theme_mod( $key ) ) )
			remove_theme_mod( $key );

	}

	$tif_themes_options = array(
			'woocommerce_default_catalog_orderby',
			'woocommerce_catalog_columns',
			'woocommerce_catalog_rows',
	);

	foreach ( $tif_themes_options as $key ) {

		if ( null != ( get_option( $key ) ) )
			delete_option( $key );

	}

}

/**
 * Tweaks Sanitize and register
 */
function tif_tweaks_sanitize( $input ) {

	// Verify that the nonce is valid.
	if ( ! isset( $_POST['tif_tweaks_nonce_field'] ) || ! wp_verify_nonce( $_POST['tif_tweaks_nonce_field'], 'tif_tweaks_action' ) ) :

		$code	 = 'nonce_unverified';
		$message = esc_html__( 'Sorry, your nonce did not verify.', 'tif-tweaks' );
		$type	 = 'error';

		return $input;

	endif;

	$code	 = 'settings_updated';
	$message = esc_html__( 'Congratulations, the data has been successfully recorded.' , 'tif-tweaks' );
	$type	 = 'updated';

	/**
	 * @link https://developer.wordpress.org/reference/functions/add_settings_error/
	 */
	add_settings_error(
		esc_attr( 'tweaks_settings_error' ),
		esc_attr( $code ),
		esc_html__( $message ),
		$type
	);

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_tweaks', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-tweaks' ) );

	// if( null== $input['tif_text'] )
	// 	add_settings_error( 'tif-tweaks', 'invalid-text', 'Text is required.' );

	$new_input =
		! empty( get_option( 'tif_plugin_tweaks' ) )
		? get_option( 'tif_plugin_tweaks' )
		: tif_plugin_tweaks_setup_data() ;

	// Last Tab used
	$new_input['tif_tabs'] =
		! empty( $input['tif_tabs'] )
		? tif_sanitize_html( $input['tif_tabs'] )
		: false ;

	if ( ! empty( $input['tif_reset_theme_mods'] ) || $new_input['tif_template_setup_data'] != $input['tif_template_setup_data'] ) {

		// Reset theme mods
		tif_reset_theme_mods();

		// Set new theme mods
		tif_template_setup_data( $input['tif_template_setup_data'] );

		// Create new theme css
		tif_create_theme_main_css( true );

		$new_input['tif_template_setup_data'] =
			! empty( $input['tif_template_setup_data'] )
			? tif_sanitize_key( $input['tif_template_setup_data'] )
			: tif_sanitize_key( $new_input['tif_template_setup_data'] ) ;

	}

	// Settings
	$restrictions = tif_get_option( 'plugin_tweaks', 'tif_init,restrictions', 'multicheck' ) ;
	if( current_user_can( 'manage_options' ) ) :
		$tif_init = array(
			'capabilities'			=> 'multicheck',
			'restrictions'			=> 'multicheck',
			'unify_menus'			=> 'checkbox',
			'menu_title'			=> 'string',
			'admin_footer'			=> 'html',
		);

		foreach ( $tif_init as $key => $value) {
			$new_input['tif_init'][$key] =
				! empty( $input['tif_init'][$key] )
				? call_user_func( 'tif_sanitize_' . $value, $input['tif_init'][$key] )
				: ( empty( $restrictions ) || in_array( get_current_user_id(), $restrictions )
					? null
					: call_user_func( 'tif_sanitize_' . $value, $new_input['tif_init'][$key] ) );
		}
	endif;

	// Restricted
	if( current_user_can( 'manage_options' ) ) :
		$tif_init = array(
			'restricted'			=> 'multicheck',
		);

		foreach ( $tif_init as $key => $value) {
			$new_input['tif_customizer'][$key] =
				! empty( $input['tif_customizer'][$key] )
				? call_user_func( 'tif_sanitize_' . $value, $input['tif_customizer'][$key] )
				: ( empty( $restrictions ) || in_array( get_current_user_id(), $restrictions )
					? null
					: call_user_func( 'tif_sanitize_' . $value, $new_input['tif_customizer'][$key] ) );
		}
	endif;

	// Customizer
	$tif_customizer = array(
		'globals'				=> 'multicheck',
		'settings'				=> 'multicheck',
		'additional'			=> 'multicheck',
		'woo'					=> 'multicheck',
		'template'				=> 'multicheck',
		'homepage_control'		=> 'multicheck',
	);

	foreach ( $tif_customizer as $key => $value) {
		$new_input['tif_customizer'][$key] =
			! empty( $input['tif_customizer'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_customizer'][$key] )
			: false ;
	}

	// Callback
	$tif_callback = array(
		// Front
		'remove_jquery'				=> 'checkbox',
		'remove_emojis'				=> 'checkbox',
		'wp_generator'				=> 'checkbox',
		'wlwmanifest_link'			=> 'checkbox',
		// 'adjacent_links'			=> 'checkbox',
		'shortlink'					=> 'checkbox',
		'disable_rest_api'			=> 'checkbox',
		'disable_rest_api_users'	=> 'checkbox',

		// Comments
		'close_comments'			=> 'multicheck',
		'invert_comments_fields'	=> 'checkbox',
		'hide_url_field'			=> 'checkbox',
		'remove_comments_class'		=> 'multicheck',

		// Authors
		'author_disabled'			=> 'key',
		'author_status'				=> 'absint',

		// SEO
		'noindex'					=> 'multicheck',
		'missing_canonical'			=> 'key',
		'comments_canonical'		=> 'checkbox',

		// Back
		'remove_wp_logo'			=> 'checkbox',
		'remove_dashboard'			=> 'multicheck',
		'login_page_logo'			=> 'url',
		'login_page_bgimg'			=> 'url',
		'login_page_bgcolor'		=> 'hexcolor',
		'lost_password_link'		=> 'checkbox',
		'hide_login_errors'			=> 'checkbox',

		// Media
		'sanitize_uploads'			=> 'checkbox',
		'images_sizes'				=> 'multicheck',

		'removed_theme_support'		=> 'multicheck'
	);

	foreach ( $tif_callback as $key => $value) {
		$new_input['tif_callback'][$key] =
			! empty( $input['tif_callback'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_callback'][$key] )
			: false ;
	}

	$tif_restricted_callback = array(
		'hide_core_updates'			=> 'multicheck',
	);

	foreach ( $tif_restricted_callback as $key => $value) {
		$new_input['tif_restricted_callback'][$key] =
			! empty( $input['tif_restricted_callback'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_restricted_callback'][$key] )
			: ( empty( $restrictions ) || in_array( get_current_user_id(), $restrictions )
				? null
				: call_user_func( 'tif_sanitize_' . $value, $new_input['tif_restricted_callback'][$key] ) );
	}

	// Disabled Feeds
	$tif_feeds = array(
		'main'						=> 'checkbox',
		'author'					=> 'checkbox',
		'category'					=> 'checkbox',
		'search'					=> 'checkbox',
		'tag'						=> 'checkbox',
		'comments'					=> 'checkbox',
		'posts_comments'			=> 'checkbox',
	);

	foreach ( $tif_feeds as $key => $value) {
		$new_input['tif_feeds'][$key] =
			! empty( $input['tif_feeds'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_feeds'][$key] )
			: false ;
	}

	return $new_input;

}
