<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Hide customizer options', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Globals settings', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,globals', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,globals', 'multicheck' ),
			'options'		=> array(
				'theme_order'		=> esc_html__( 'Elements order', 'tif-tweaks' ),
				'theme_alignment'	=> esc_html__( 'Theme alignments', 'tif-tweaks' ),
				'theme_fonts'		=> esc_html__( 'Fonts Settings', 'tif-tweaks' ),
				'theme_colors'		=> esc_html__( 'Colors Settings', 'tif-tweaks' ),
				'theme_images'		=> esc_html__( 'Images Settings', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_customizer][globals]'
	);

	$form->add_input( esc_html__( 'Theme Settings', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,settings', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,settings', 'multicheck' ),
			'options'		=> array(
				'theme_init'		=> esc_html__( 'Basic settings', 'tif-tweaks' ),
				'theme_header'		=> esc_html__( 'Header', 'tif-tweaks' ),
				'theme_navigation'	=> esc_html__( 'Navigation', 'tif-tweaks' ),
				'theme_breadcrumb'	=> esc_html__( 'Breadcrumb', 'tif-tweaks' ),
				'theme_title_bar'	=> esc_html__( 'Title bar', 'tif-tweaks' ),
				'theme_loop'		=> esc_html__( 'Main loop', 'tif-tweaks' ),
				// 'theme_privacy'		=> esc_html__( 'Privacy policy', 'tif-tweaks' ),
				// 'theme_debug'		=> esc_html__( 'Debug settings', 'tif-tweaks' ),
				// 'theme_credit'		=> esc_html__( 'Credit settings', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_customizer][settings]'
	);


	$form->add_input( esc_html__( 'Components settings', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,additional', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,additional', 'multicheck' ),
			'options'		=> array(
				'theme_post_cover'		=> esc_html__( 'Post Cover layout', 'tif-tweaks' ),
				'theme_post_related'	=> esc_html__( 'Related posts', 'tif-tweaks' ),
				'theme_post_child'		=> esc_html__( 'Child posts', 'tif-tweaks' ),
				'theme_post_taxonomies'	=> esc_html__( 'Taxonomies', 'tif-tweaks' ),
				'theme_post_share'		=> esc_html__( 'Share buttons', 'tif-tweaks' ),

				'theme_buttons'			=> esc_html__( 'Buttons', 'tif-tweaks' ),
				'theme_alerts'			=> esc_html__( 'Alerts', 'tif-tweaks' ),
				'theme_quotes'			=> esc_html__( 'Quotes', 'tif-tweaks' ),
				'theme_social'			=> esc_html__( 'Social links', 'tif-tweaks' ),
				'theme_pagination'		=> esc_html__( 'Pagination', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_customizer][additional]'
	);

	if ( empty( $restrictions ) || in_array( get_current_user_id(), $restrictions ) ) {

		$form->add_input( esc_html__( 'Restricted settings', 'tif-tweaks' ),
			array(
				'type'			=> 'checkbox',
				'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,restricted', 'multicheck' ),
				'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,restricted', 'multicheck' ),
				'description'	=> esc_html__( 'These restricted settings are only accessible to authorized administrators if this is specified in the Setup tab.', 'tif-tweaks' ),
				'options'		=> array(
					'theme_utils'		=> esc_html__( 'Generated utilities class', 'tif-tweaks' ),
					'theme_assets'		=> esc_html__( 'CSS & JS Settings', 'tif-tweaks' ),
					'theme_privacy'		=> esc_html__( 'Privacy policy', 'tif-tweaks' ),
					'theme_debug'		=> esc_html__( 'Debug settings', 'tif-tweaks' ),
					'theme_credit'		=> esc_html__( 'Credit settings', 'tif-tweaks' ),
				),
			),
			$tif_plugin_name . '[tif_customizer][restricted]'
		);

	}

	$form->add_input( esc_html__( 'WooCommerce', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,woo', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,woo', 'multicheck' ),
			'options'		=> array(
				'theme_woo'		=> esc_html__( 'Woo settings', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_customizer][woo]'
	);

	$form->add_input( esc_html__( 'Template', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,template', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,template', 'multicheck' ),
			'options'		=> array(
				'theme_contact'		=> esc_html__( 'Contact settings', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_customizer][template]'
	);

	$form->add_input( esc_html__( 'Homepage', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_customizer,homepage_control', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_customizer,homepage_control', 'multicheck' ),
			'options'		=> array(
				'homepage_control'	=> esc_html__( 'Homepage order', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_customizer][homepage_control]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );
