<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Display', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Remove Wordpress logo', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		 => tif_get_option( 'plugin_tweaks', 'tif_callback,remove_wp_logo', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][remove_wp_logo]'
	);

	$form->add_input( esc_html__( 'Remove dashboard widgets', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_callback,remove_dashboard', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_callback,remove_dashboard', 'multicheck' ),
			'options'		=> array(
				'welcome_panel'	=> esc_html__( 'Welcome panel', 'tif-tweaks' ),
				'right_now'		=> esc_html__( '"At a Glance"', 'tif-tweaks' ),
				'activity'		=> esc_html__( '"Activity" which includes "Recent Comments"', 'tif-tweaks' ),
				'quick_press'	=> esc_html__( '"Quick Draft" which includes "Your Recent Drafts"', 'tif-tweaks' ),
				'primary'		=> esc_html__( '"WordPress Events and News"', 'tif-tweaks' ),
			),
		),
		$tif_plugin_name . '[tif_callback][remove_dashboard]'
	);

	$form->add_input( esc_html__( 'Change the Wordpress logo on the login page', 'tif-tweaks' ),
		array(
			'type'            => 'text',
			'mediauploader'   => true,
			'value'           => tif_get_option( 'plugin_tweaks', 'tif_callback,login_page_logo', 'url' ),
		),
		$tif_plugin_name . '[tif_callback][login_page_logo]'
	);

	// $form->add_input( esc_html__( 'Add a background image on the login page', 'tif-tweaks' ),
	// 	array(
	// 		'type'            => 'text',
	// 		'mediauploader'   => true,
	// 		'value'           => tif_get_option( 'plugin_tweaks', 'tif_callback,login_page_bgimg', 'url' ),
	// 	),
	// 	$tif_plugin_name . '[tif_callback][login_page_bgimg]'
	// );

	$form->add_input( esc_html__( 'Change background color on the login page', 'tif-tweaks' ),
		array(
			'type'            => 'text',
			'colorpicker'      => true,
			'value'           => tif_get_option( 'plugin_tweaks', 'tif_callback,login_page_bgcolor', 'hexcolor' ),
			'default'         => tif_get_default( 'plugin_tweaks', 'tif_callback,login_page_bgcolor', 'hexcolor' ),
		),
		$tif_plugin_name . '[tif_callback][login_page_bgcolor]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Security', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Hide &quot;Forgot your password&quot; link on login page', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		 => tif_get_option( 'plugin_tweaks', 'tif_callback,lost_password_link', 'checkbox' ),
			'description'	 => esc_html__( 'Remove lost password link from WordPress login Page', 'tif-tweaks' )
		),
		$tif_plugin_name . '[tif_callback][lost_password_link]'
	);

	$form->add_input( esc_html__( 'Hide connection errors', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> 1,
			'checked'		 => tif_get_option( 'plugin_tweaks', 'tif_callback,hide_login_errors', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_callback][hide_login_errors]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Theme support', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Removed Theme support', 'tif-tweaks' ),
		array(
			'type'			=> 'checkbox',
			'value'			=> tif_get_option( 'plugin_tweaks', 'tif_callback,removed_theme_support', 'multicheck' ),
			'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_callback,removed_theme_support', 'multicheck' ),
			'options'		=> array(
				'custom_header'			=> esc_html__( 'Custom header', 'tif-tweaks' ),
				'custom_background'		=> esc_html__( 'Custom background', 'tif-tweaks' ),
				'widgets_block_editor'	=> esc_html__( 'Widgets block editor', 'tif-tweaks' ),
				'core_block_patterns'	=> esc_html__( 'Core block patterns', 'tif-tweaks' ),
			),
			'description'	=> sprintf( '<strong>%s</strong> : %s <br><strong>%s</strong> : %s ',
				esc_html__( 'Classic Widgets', 'tif-tweaks' ),
				esc_html__( 'Restore the previous widgets settings screens and disable the block editor from managing widgets.', 'tif-tweaks' ),
				esc_html__( 'Core block patterns', 'tif-tweaks' ),
				esc_html__( 'Remove core block patterns.', 'tif-tweaks' )
			)
		),
		$tif_plugin_name . '[tif_callback][removed_theme_support]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );
