<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if ( empty( $restrictions ) || in_array( get_current_user_id(), $restrictions ) ) {

	$form->add_input( 'html' . $count++ , array(
		'type' => 'html',
		'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Authorized users of this tab', 'tif-tweaks' ) . '</legend>'
	) );

		$form->add_input(
			esc_html__( 'Allowed roles', 'tif-tweaks' ),
			array(
				'type'			=> 'checkbox',
				'is_admin'		=> true,
				'value'			=> tif_get_option( 'plugin_tweaks', 'tif_init,restrictions', 'multicheck' ),
				'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_init,restrictions', 'multicheck' ),
				'options'		=> tif_get_administrators_list(),
				'description'	=> esc_html__( 'Restrict access to this tab to selected administrators. If none is checked, all will have access to this settings.', 'tif-tweaks' )
			),
			$tif_plugin_name . '[tif_init][restrictions]'
		);

	$form->add_input( 'html' . $count++, array(
		'type' => 'html',
		'value' => '</fieldset>'
	) );

	$form->add_input( 'html' . $count++ , array(
		'type' => 'html',
		'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Setup', 'tif-tweaks' ) . '</legend>'
	) );

		$form->add_input( esc_html__( 'Menu title', 'tif-tweaks' ),
			array(
				'type'			=> 'text',
				'value'			=> tif_get_option( 'plugin_tweaks', 'tif_init,menu_title', 'text' ),
				'info'			=> esc_html__( 'Here you can customize the title that will be displayed in the menu.', 'tif-tweaks' )
			),
			$tif_plugin_name .'[tif_init][menu_title]'
		);


		$form->add_input(
			esc_html__( 'Unify "Themes In France" plugins menus', 'tif-tweaks' ),
			array(
				'type'			=> 'checkbox',
				'value'			=> 1,
				'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_init,unify_menus', 'unify_menus' ),
				'description'	=> esc_html__( 'Move the submenus of the "themes in france" plugins to the "canopee" menu.', 'tif-tweaks' ),
			),
			$tif_plugin_name . '[tif_init][unify_menus]'
		);

		$form->add_input(
			esc_html__( 'Change the Wordpress admin footer text', 'tif-tweaks' ),
			array(
				'type'			=> 'textarea',
				// 'required'		=> true,
				'value'			=> tif_get_option( 'plugin_tweaks', 'tif_init,admin_footer', 'html' ),
			),
			$tif_plugin_name . '[tif_init][admin_footer]'
		);

	$form->add_input( 'html' . $count++, array(
		'type' => 'html',
		'value' => '</fieldset>'
	) );

	$form->add_input( 'html' . $count++ , array(
		'type' => 'html',
		'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Core updates', 'tif-tweaks' ) . '</legend>'
	) );

		$form->add_input(
			esc_html__( 'Hide core updates', 'tif-tweaks' ),
			array(
				'type'			=> 'checkbox',
				'is_admin'		=> true,
				'value'			=> tif_get_option( 'plugin_tweaks', 'tif_restricted_callback,hide_core_updates', 'multicheck' ),
				'checked'		=> tif_get_default( 'plugin_tweaks', 'tif_restricted_callback,hide_core_updates', 'multicheck' ),
				'options'		=> array(
					'core'			=> esc_html__( 'Core', 'tif-tweaks' ),
					'themes'		=> esc_html__( 'Themes', 'tif-tweaks' ),
					'plugins'		=> esc_html__( 'Plugins', 'tif-tweaks' ),
				),
				'description'	=> esc_html__( 'Hide updates to non-administrators or to those who are not checked above.', 'tif-tweaks' )
			),
			$tif_plugin_name . '[tif_restricted_callback][hide_core_updates]'
		);

	$form->add_input( 'html' . $count++, array(
		'type' => 'html',
		'value' => '</fieldset>'
	) );

	$form->add_input( 'html' . $count++ , array(
		'type'	=> 'html',
		'value'	=> '<fieldset>'."\n".'<legend>' . esc_html__( 'Theme mods', 'tif-tweaks' ) . '</legend>'
	) );

		$form->add_input(
			esc_html__( 'Reset', 'tif-tweaks' ),
			array(
				'type'			=> 'checkbox',
				'value'			=> 1,
				'checked'		=> '',
				'description'	=> esc_html__( 'Reset all custom options and return to default settings.', 'tif-tweaks' ),
			),
			$tif_plugin_name . '[tif_reset_theme_mods]'
		);

		$form->add_input(
			esc_html__( 'Predefined layouts', 'tif-tweaks' ),
			array(
				'type'			=> 'radio',
				'is_img'		=> true,
				'checked'		=> tif_get_option( 'plugin_tweaks', 'tif_template_setup_data', 'key' ),
				'options'		=> array(
					'default'		=> array( TIF_TWEAKS_ADMIN_IMAGES_URL . '/template-default.png', esc_html__( 'Default', 'tif-tweaks' ) ),
					'simple'		=> array( TIF_TWEAKS_ADMIN_IMAGES_URL . '/template-simple.png', esc_html__( 'Simple', 'tif-tweaks' ) ),
					'magazine'		=> array( TIF_TWEAKS_ADMIN_IMAGES_URL . '/template-magazine.png', esc_html__( 'Magazine', 'tif-tweaks' ) ),
					'blog'			=> array( TIF_TWEAKS_ADMIN_IMAGES_URL . '/template-blog.png', esc_html__( 'Blog', 'tif-tweaks' ) ),
				),
			),
			$tif_plugin_name . '[tif_template_setup_data]'
		);

	$form->add_input( 'html' . $count++, array(
		'type'	=> 'html',
		'value'	=> '</fieldset>'
	) );

} else {

	$form->add_input( 'html' . $count++ , array(
		'type'	=> 'html',
		'value'	=> '<fieldset class="restricted-area">'."\n".'<legend>' . esc_html__( 'Restricted area', 'tif-tweaks' ) . '</legend>'
	) );

	$form->add_input( 'html' . $count++, array(
		'type' => 'html',
		'value' => '<p>' . esc_html__( 'Unauthorized user.', 'tif-tweaks' ) . '</p>'
	) );

	$form->add_input( 'html' . $count++, array(
		'type' => 'html',
		'value' => '</fieldset>'
	) );

}
