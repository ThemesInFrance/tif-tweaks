<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<div class="tif-help"><fieldset>'."\n".'<legend>' . esc_html__( 'Crédits', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Canopee', 'tif-tweaks' ),
		array(
			'type' => 'content',
			'value' => wp_kses( tif_canopee_credits(), tif_allowed_html() ),
		)
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset></div>'
) );
