<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_theme_options_page() {

	$capability		= tif_get_submenu_capability( tif_get_option( 'plugin_tweaks', 'tif_init,capabilities', 'multicheck' ) ) ;
	$restrictions	= tif_get_option( 'plugin_tweaks', 'tif_init,restrictions', 'multicheck' ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-tweaks' ) );

	?>

	<div class="wrap">

		<h1 class="tif-plugin-title"><?php echo Tif_Tweaks_Init::tif_tweaks_menu_title(); ?></h1>
		<p><?php _e( 'This plugin gathers some lines of code frequently used to secure its website or improve its performance. It also proposes some optimizations specific to the Canopée theme.', 'tif-tweaks' ) ?></p>

		<form method="post" action="options.php" class="tif-form tif-plugin-metabox row tif-tabs">

			<?php

			settings_fields( 'tif-tweaks' );
			// do_settings_sections( 'tif-tweaks' );
			wp_nonce_field( 'tif_tweaks_action', 'tif_tweaks_nonce_field' );
			$tif_plugin_name = 'tif_plugin_tweaks';

			// Create a new instance
			$form = new Tif_Form_Builder();

			$count = 0;
			$tabs = 0;

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'class', 'tif-form tif-tabs' );
			$form->set_att( 'novalidate', false );
			$form->set_att( 'is_array', 'tif_plugin_tweaks' );
			$form->set_att( 'form_element', false );
			$form->set_att( 'add_submit', true );

			/**
			 * New tab for Front Office options
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Front Office', 'tif-tweaks' ),
				'dashicon' => 'dashicons-wordpress-alt',
				'first' => true,
			) );

				require_once 'tif-settings-tab-front.php';

			/**
			 * New tab for back Office options
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Back Office', 'tif-tweaks' ),
				'dashicon' => 'dashicons-screenoptions',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-back.php';

			/**
			 * New tab for Media options
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Media', 'tif-tweaks' ),
				'dashicon' => 'dashicons-admin-media',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-media.php';

			/**
			 * New tab for Customizer options
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Customizer', 'tif-tweaks' ),
				'dashicon' => 'dashicons-admin-appearance',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-customizer.php';

			/**
			 * New tab for layout settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Setup', 'tif-tweaks' ),
				'dashicon' => 'dashicons-art',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-setup.php';

			/**
			 * New tab for help
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Help', 'tif-tweaks' ),
				'dashicon' => 'dashicons-editor-help',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-help.php';

			/**
			 * New tab for administrator settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_tweaks', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Credits', 'tif-tweaks' ),
				'dashicon' => 'dashicons-smiley',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

				require_once 'tif-settings-tab-credits.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</div>',
			) );

			// Create the form
			$form->build_form();

			?>

		</form>

	</div><!-- .wrap -->

	<?php

	// echo wp_kses( tif_plugin_powered_by(), tif_allowed_html() );
	echo wp_kses( tif_memory_usage(), tif_allowed_html() );

}
