<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<div class="tif-help"><fieldset>'."\n".'<legend>' . esc_html__( 'Help', 'tif-tweaks' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Need some help ?', 'tif-tweaks' ),
		array(
			'type' => 'content',
			'value' => wp_kses( tif_plugin_help(), tif_allowed_html() ),
		)
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset></div>'
) );
