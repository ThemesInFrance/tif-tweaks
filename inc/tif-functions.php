<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_get_administrators_list(){

	$administrators = get_users( array(
 		'role'		=> 'administrator',
 		'orderby'	=> 'user_nicename',
 		'order'		=> 'ASC'
 	));

	foreach ( $administrators as $user ) {
		 $roles_translated[$user->ID] = $user->display_name;
	}

	return $roles_translated;
}

// FRONT
/**
 * Disable jQuery
 */
function tif_remove_jquery() {

	if ( current_user_can( 'administrator' ) )
		return;

	add_action( 'wp_enqueue_scripts', 'tif_deregister_script' );

}

function tif_deregister_script() {

	wp_deregister_script( 'jquery' );

}

/**
 * Disable Emojis
 */
function tif_remove_emojis() {

	add_filter( 'emoji_svg_url', '__return_false' ); // remove <link rel='dns-prefetch' href='//s.w.org' />
	add_action( 'init', 'tif_disable_wp_emojis' );
	add_filter( 'tiny_mce_plugins', 'tif_disable_tinymce_emojis' );

}

// add_filter( 'wp_resource_hints', 'tif_emojis_remove_dns_prefetch', 10, 2 );
// all actions related to emojis
function tif_disable_wp_emojis() {

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

}

// filter to remove TinyMCE emojis
function tif_disable_tinymce_emojis( $plugins ) {

	if ( is_array( $plugins ) )
		return array_diff( $plugins, array( 'wpemoji' ) );

	else
		return array();

}

/*
 * Supprimer les versions de Wordpress et Woocommerce dans le header
 *
**/
function tif_wp_generator() {
	remove_action( 'wp_head', 'wp_generator' );
}

function tif_wlwmanifest_link() {
	// Permettent d’interagir avec Windows Live Writer. Si vous ne vous en servez pas, vous pouvez supprimer.
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rsd_link' );
}

function tif_adjacent_links() {
	// supprimer la balise rel=index
	remove_action( 'wp_head', 'index_rel_link' );

	// supprimer la balise rel=shortlink
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

	// supprime le lien vers la catégorie parente
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0);
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0);
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0);
	// Affiche les liens relatifs vers les articles suivants et précédents.
	// <kbd><link title=" href=" rel="prev" /></kbd> et
	// <kbd>	<link title="" href=""" rel="next" /></kbd>.
	// Ces liens peuvent parfois affecter votre SEO.
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
}

function tif_shortlink() {

	// supprimer la balise rel=shortlink
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

}

function tif_sanitize_uploads() {
	add_filter( 'sanitize_file_name_chars', 'tif_sanitize_filename_chars', 10, 1 );
	add_filter( 'sanitize_file_name', 'tif_sanitize_filename', 10, 1 );
}

function tif_images_sizes() {
	global $sizes;
	add_action('intermediate_image_sizes_advanced', function( $sizes ){
		$tif_wp_images_sizes_disabled = tif_get_option( 'plugin_tweaks', 'tif_callback,images_sizes', 'multicheck' );
		foreach ($tif_wp_images_sizes_disabled as $key) {
			unset( $sizes[ $key ] );
		}
	return $sizes;
	}, 10, 3 );
}

function tif_removed_theme_support() {
	add_action('after_setup_theme', function(){
		$removed = tif_get_option( 'plugin_tweaks', 'tif_callback,removed_theme_support', 'multicheck' );
		foreach ( $removed as $slug ) {
			remove_theme_support( tif_sanitize_slug( $slug ) );
		}
	}, 100 );
}

function tif_sanitize_filename_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '‹', '›', '—' ), $special_chars );

	return $special_chars;
}

function tif_remove_main_feed_link() {
	add_filter( 'feed_links_show_posts_feed', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}

function tif_remove_author_feed_link() {
	add_filter( 'author_feed_link', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}

function tif_remove_category_feed_link() {
	add_filter( 'category_feed_link', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}

function tif_remove_search_feed_link() {
	add_filter( 'search_feed_link', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}

function tif_remove_tag_feed_link() {
	add_filter( 'tag_feed_link', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}

function tif_remove_posts_comments_feed_link() {
	// Removes Post Comments Feed
	add_filter( 'post_comments_feed_link', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}

function tif_remove_comments_feed_link() {
	// Removes global Comments Feed
	add_filter( 'feed_links_show_comments_feed', '__return_false' );
	add_action( 'parse_query', 'tif_feed_links_404' );
}
function tif_feed_links_404( $object ) {

	if ( ! $object->is_feed )
		return;

	$disabled_feeds = array_filter (tif_get_option( 'plugin_tweaks', 'tif_feeds', 'array' ) );

	foreach ( $disabled_feeds as $key => $value) {

		$die_message = 'Page not found.';
		$die_array = array( 'response'  => 404, 'back_link' => true );

		if ( 	 $object->is_feed
			&& ! $object->is_author
			&& ! $object->is_category
			&& ! $object->is_search
			&& ! $object->is_tag
			&& ! $object->is_comment_feed
			&& ! $object->is_single
			&& $key == 'main' )
			wp_die( $die_message . '(main)', '', $die_array);

		if ( $object->is_feed && $object->is_author  && $key == 'author' )
			wp_die( $die_message . '(author)', '', $die_array);

		if ( $object->is_feed && $object->is_category  && $key == 'category' )
			wp_die( $die_message . '(category)', '', $die_array);

		if ( $object->is_feed && $object->is_search  && $key == 'search' )
			wp_die( $die_message . '(search)', '', $die_array);

		if ( $object->is_feed && $object->is_tag  && $key == 'tag' )
			wp_die( $die_message . '(tag)', '', $die_array);

		if ( $object->is_comment_feed && ! $object->is_single && $key == 'comments' )
			wp_die( $die_message . '(comments)', '', $die_array);

		if ( $object->is_comment_feed && $object->is_single && $key == 'posts_comments' )
			wp_die( $die_message . '(posts_comments)', '', $die_array);

	}
}

// BACK
/**
 * Désactiver la fonction mot de passe oublié
 */

function tif_lost_password_link() {
	add_filter( 'show_password_fields', 'tif_is_admin' );
	add_filter( 'allow_password_reset', 'tif_is_admin' );
	add_filter( 'gettext', 'tif_remove_lost_password_link' );
}

function tif_is_admin() {
	return is_admin() && current_user_can( 'administrator' );
}

function tif_remove_lost_password_link( $text ) {
	return str_replace(
		array(
			'Lost your password?',
			'Lost your password',
			'Mot de passe oublié'
		),
		'',
		trim($text, '?' ) );
}

function tif_hide_login_errors() {
	add_action( 'user_profile_update_errors', 'tif_no_password_change', 10, 3 );
	add_filter( 'login_errors','tif_new_login_errors_message' );
}


function tif_no_password_change( $dummy1, $dummy2, $user ) {
	if( ! tif_is_admin() )
		unset( $user->user_pass );
}

function tif_new_login_errors_message() {
	return sprintf( '%1$s<br />%2$s',
			esc_html__( 'An error has occurred.', 'tif-tweaks' ),
			esc_html__( 'Please contact your administrator if the problem persists.', 'tif-tweaks' )
		);
}

/**
 * Deactivate REST-API
 * @return void
 */
function tif_disable_rest_api() {

	if ( is_user_logged_in() )
		return;

	remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
	remove_action( 'wp_head', 'rest_output_link_wp_head' );
	remove_action( 'template_redirect', 'rest_output_link_header', 11 );

	global $wp_version;

	// WordPress 4.7+ disables the REST API via authentication short-circuit.
	// For versions of WordPress < 4.7, disable the REST API via filters
	if ( version_compare( $wp_version, '4.7', '>=' ) ) {
		// Load the primary Disable_REST_API class
		require_once( TIF_TWEAKS_DIR . 'inc/class/Tif_Disable_Rest_Api.php' );
		new \Tif_Disable_Rest_Api();
	} else {

		remove_action( 'init', 'rest_api_init' );
		remove_action( 'parse_request', 'rest_api_loaded' );

		remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status' );
		remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status' );

		// Filters for WP-API version 1.x
		add_filter( 'json_enabled', '__return_false' );
		add_filter( 'json_jsonp_enabled', '__return_false' );

		// Filters for WP-API version 2.x
		add_filter( 'rest_enabled', '__return_false' );
		add_filter( 'rest_jsonp_enabled', '__return_false' );
	}

}
/**
 * Disable REST API user endpoints
 * @see https://fr.wordpress.org/plugins/smntcs-disable-rest-api-user-endpoints/
 * @return void
 */
function tif_disable_rest_api_users() {

	if ( is_user_logged_in() )
		return;

	add_filter( 'rest_endpoints', function ( $endpoints ) {
		if ( isset( $endpoints['/wp/v2/users'] ) ) {
			unset( $endpoints['/wp/v2/users'] );
		}
		if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
			unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
		}

		return $endpoints;
	} );

}


function tif_remove_wp_logo() {

	add_action('admin_bar_menu', function(){
		global $wp_admin_bar;
		$wp_admin_bar->remove_node( 'wp-logo' );
	}, 999);

}

function tif_remove_dashboard() {
	add_action( 'wp_dashboard_setup', function(){

	$removed = tif_get_option( 'plugin_tweaks', 'tif_callback,remove_dashboard', 'multicheck' );

	$dashboard = array(
		'right_now' => 'normal',	// "At a Glance"
		'activity' => 'normal',		// "Activity" which includes "Recent Comments"
		'quick_press' => 'side',	// "Quick Draft" which includes "Your Recent Drafts"
		'primary' => 'side'			// "WordPress Events and News"
	);

	foreach ( $removed as $id ) {

		if ( $id == 'welcome_panel' )
			remove_action ( 'welcome_panel','wp_welcome_panel' );

		else
			remove_meta_box( 'dashboard_' . $id, 'dashboard', $dashboard[$id] );

	}

	/**
	 * Woocommerce dashboard
	 */
	// remove_meta_box( 'woocommerce_dashboard_recent_reviews', 'dashboard', 'side' );	// WooCommerce "Recent Comments"
	// remove_meta_box( 'woocommerce_dashboard_status', 'dashboard', 'side' );			// WooCommerce "Recent Comments"
	});

}

function tif_login_page_logo() {

	add_action('login_head', function(){

	$logo = tif_get_option( 'plugin_tweaks', 'tif_callback,login_page_logo', 'url' );

	echo "<style>
	.login h1 a {
		background-image: url('" . esc_url( $logo ) . "');
		width: 100%;
		background-size: contain;
		background-repeat: no-repeat;
	}
	</style>";

	});

	add_filter( 'login_headerurl', function(){
		return get_bloginfo( 'url' );
	});

}

function tif_login_page_bgimg() {

	add_action('login_head', function(){

	$bgimg = tif_get_option( 'plugin_tweaks', 'tif_callback,login_page_bgimg', 'url' );

	echo "<style>
	body {
		background-image: url('" . esc_url( $bgimg ) . "');
		background-size: cover;
		background-repeat: no-repeat;
	}
	</style>";

	});

	add_filter( 'login_headerurl', function(){
		return get_bloginfo( 'url' );
	});

}

function tif_login_page_bgcolor() {

	add_action('login_head', function(){

	$bgcolor = tif_get_option( 'plugin_tweaks', 'tif_callback,login_page_bgcolor', 'url' );

	echo "<style>
	body {
		background-color: " . tif_sanitize_hexcolor( $bgcolor ) . ";
	}
	</style>";

	});

	add_filter( 'login_headerurl', function(){
		return get_bloginfo( 'url' );
	});

}

$admin_footer = tif_get_option( 'plugin_tweaks', 'tif_init,admin_footer', 'html' );
if ( null != $admin_footer )
	add_filter( 'admin_footer_text', function(){

			echo tif_get_option( 'plugin_tweaks', 'tif_init,admin_footer', 'html' );

	}
);

// === COMMENTS ================================================================

/*
 * Close comments
**/
function tif_close_comments() {
	add_filter( 'comments_open', 'tif_comments_open', 10, 2 );
}
function tif_comments_open( $open, $post_id ) {

	$open = true;

	$comments = tif_get_option( 'plugin_tweaks', 'tif_callback,close_comments', 'multicheck' );

	if ( is_single() && in_array( 'post', $comments ) )
		$open = false;

	if ( is_page() && in_array( 'page', $comments ) )
		$open = false;

	if ( is_attachment() && in_array( 'attachment', $comments ) )
		$open = false;

	return $open;

}

/*
 * Move comment text field to bottom
**/
function tif_invert_comments_fields() {
	add_filter( 'comment_form_fields', 'tif_move_comment_field_to_bottom' );
}
function tif_move_comment_field_to_bottom( $fields ) {

	if ( ! tif_get_option( 'plugin_tweaks', 'tif_callback,invert_comments_fields' ) )
		return $fields;

	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;

	return $fields;

}


/*
 * Hide url field
**/
function tif_hide_url_field() {
	add_filter( 'comment_form_default_fields', 'tif_remove_url_field' );
}
function tif_remove_url_field( $fields ) {

	if ( ! tif_get_option( 'plugin_tweaks', 'tif_callback,hide_url_field' ) )
		return;

	if( isset( $fields['url'] ) )
		unset( $fields['url'] );

	return $fields;

}

/*
 * Remove comments class
 * @link https://www.zigpress.com/2015/08/05/how-to-remove-the-security-hole-in-wordpress-comment-html/
**/
function tif_remove_comments_class() {

	add_filter( 'comment_class', 'tif_filter_comments_class' );

}
function tif_filter_comments_class( $classes ) {

	if ( ! $classes )
		return;

	$removed_class = tif_get_option( 'plugin_tweaks', 'tif_callback,remove_comments_class', 'multicheck' );

	if ( is_array( $classes ) && count( $classes ) >= 1 ) {

		foreach ( $classes as $key => $class ) {

			if ( in_array( 'byuser', $removed_class ) && $class == 'byuser' )
				unset( $classes[$key] );

			if ( in_array( 'bypostauthor', $removed_class ) && $class == 'bypostauthor' )
				unset( $classes[$key] );

			if ( in_array( 'commentauthor', $removed_class ) && substr( $class, 0, 15 ) == 'comment-author-' )
				unset( $classes[$key] );

		}

	}

	return $classes;

}

// add_filter( 'comment_class', 'tif_remove_comments_class' );
// function tif_remove_comments_class( $classes ) {
//
// 	if ( is_array( $classes ) ) {
//
// 		if ( count( $classes ) >= 1 ) {
//
// 			foreach ( $classes as $key => $class ) {
//
// 				if ( $class == 'bypostauthor' ) {
// 					unset( $classes[$key] );
// 				}
//
// 				if ( $class == 'byuser' ) {
// 					unset( $classes[$key] );
// 				}
//
// 				if ( substr( $class, 0, 15 ) == 'comment-author-' ) {
// 					unset( $classes[$key] );
// 				}
//
// 			}
//
// 		}
//
// 	}
//
// 	return $classes;
// }

function tif_hide_core_updates() {

	$restrictions	= tif_get_option( 'plugin_tweaks', 'tif_init,restrictions', 'multicheck' ) ;
	$restricted		= empty( $restrictions ) || in_array( get_current_user_id(), $restrictions ) ? false : true;
	$hidden			= tif_get_option( 'plugin_tweaks', 'tif_restricted_callback,hide_core_updates', 'multicheck' );

	if ( empty( $hidden ) )
		return;

	foreach ( $hidden as $key ) {

		if ( ! current_user_can( 'update_core' ) || $restricted ) {
			add_filter( 'pre_site_transient_update_' . $key, 'tif_remove_core_updates' ); // Masque les mises à jour de WordPress
		}

	}

}
function tif_remove_core_updates(){

	global $wp_version;

	return(object) array('last_checked'=> time(),
		'version_checked'=> $wp_version,
	);

}

function tif_noindex() {

	$tif_template_name		= get_option( 'template' );
	$tif_template_name		= preg_replace("/\W/", "_", strtolower( $tif_template_name ) );

	if ( $tif_template_name == 'canopee' )
		return;

	add_action('wp_head', function(){

	$no_index = tif_get_option( 'plugin_tweaks', 'tif_callback,noindex', 'multicheck' );
	$no_index = null == $no_index ? array() : $no_index;

	if (   is_author() && in_array( 'author', $no_index )
		|| is_category() && in_array( 'category', $no_index )
		|| is_tag() && in_array( 'tag', $no_index )
		|| is_search() && in_array( 'search', $no_index )
		|| is_date() && in_array( 'date', $no_index )
		)
		echo '<meta name="robots" content="noindex,follow">' . "\n";

	});

}

function tif_author_disabled() {

	add_action('wp', function(){

		$settings = tif_get_option( 'plugin_tweaks', 'tif_callback', 'array' );

		if ( null == $settings['author_disabled'] )
			return;

		global $wp_query;

		// If an author page is requested
		if ( $wp_query->is_author ) {

			if ( ( $settings['author_disabled'] == 'unused' && count_user_posts(get_the_author_meta( 'ID' ) ) == 0 ) || $settings['author_disabled'] == 'all' ) {

				if ( $settings['author_status'] == 301 ) {

					// redirects to the home page...
					wp_safe_redirect( get_bloginfo( 'url' ), 301 );
					exit;

				} else {

					// ... or get 404
					$wp_query->set_404();
					status_header( 404 );

				}

			}

		}

	});

}


// add_action( 'wp', 'tif_tweaks_disable_author_pages' );
// function tif_tweaks_disable_author_pages () {
// 	global $wp_query;
// 	$disabled = apply_filters( 'tif_tweaks_disable_author_pages', true );
//
// 	if ( $disabled && $wp_query->is_author() ) {
// 		$wp_query->set_404();
// 		status_header( 404 );
// 	}
// }
//
// add_action( 'wp', 'tif_tweaks_disable_author_query' );
// function tif_tweaks_disable_author_query () {
// 	global $wp_query;
// 	$disabled = apply_filters( 'tif_tweaks_disable_author_query', true );
//
// 	if ( $disabled && isset( $_GET['author'] ) ) {
// 		$wp_query->set_404();
// 		status_header( 404 );
// 	}
// }
