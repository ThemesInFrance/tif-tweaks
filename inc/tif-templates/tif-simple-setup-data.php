<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_template_setup_data( $template = 'default' ) {

	if ( $template == 'default' )
		return;

	$template_setup_data = 'tif_template_' . tif_sanitize_key( $template ) . '_setup_data';

	if ( ! function_exists( $template_setup_data ) )
		return;

	$template_setup_data = $template_setup_data();

	foreach ( $template_setup_data as $key => $value ) {
		remove_theme_mod( $key );
		set_theme_mod( $key, $value );
	}

}

function tif_template_default_setup_data() {

	return null;

}

function tif_template_simple_setup_data() {

	return array(

		// GLOBALS
		'tif_theme_order' => array (
		  'tif_post_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_thumbnail:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_pagination:1',
		    5 => 'post_tags:0',
		    6 => 'post_share:1',
		    7 => 'post_navigation:1',
		    8 => 'post_related:1',
		    9 => 'post_author:0',
		    10 => 'post_comments:0',
		    11 => 'site_content_bottom:0',
		  ),
		  'tif_page_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_thumbnail:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_pagination:1',
		    5 => 'post_tags:1',
		    6 => 'post_share:1',
		    7 => 'post_author:1',
		    8 => 'post_comments:1',
		    9 => 'site_content_bottom:0',
		  ),
		  'tif_taxonomy_entry_order' =>
		  array (
		    0 => 'taxonomy_thumbnail:0',
		    1 => 'taxonomy_description:0',
		  ),
		  'tif_post_meta_order' =>
		  array (
		    0 => 'meta_avatar:0',
		    1 => 'meta_author:1',
		    2 => 'meta_published:1',
		    3 => 'meta_modified:1',
		    4 => 'meta_category:1',
		    5 => 'meta_comments:1',
		  ),
		  'tif_page_meta_order' =>
		  array (
		    0 => 'meta_avatar:1',
		    1 => 'meta_author:1',
		    2 => 'meta_published:1',
		    3 => 'meta_modified:1',
		    4 => 'meta_comments:1',
		  ),
		  'tif_attachment_entry_order' =>
		  array (
		    0 => 'post_title:0',
		    1 => 'post_attachment:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_share:1',
		    5 => 'post_author:1',
		    6 => 'post_comments:1',
		    7 => 'site_content_bottom:0',
		  ),
		  'tif_attachment_meta_order' =>
		  array (
		    0 => 'meta_avatar:0',
		    1 => 'meta_author:1',
		    2 => 'meta_published:1',
		    3 => 'meta_modified:1',
		    4 => 'meta_comments:1',
		  ),
		  'tif_404_order' =>
		  array (
		    0 => 'title:0',
		    1 => '404_number:1',
		    2 => '404_searchform:1',
		    3 => '404_posts:0',
		    4 => '404_categories:0',
		    5 => '404_archives:0',
		    6 => '404_tags:0',
		  ),
		  'tif_sitemap_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'sitemap_feeds:1',
		    2 => 'sitemap_posts:1',
		    3 => 'sitemap_categories:1',
		    4 => 'sitemap_pages:1',
		  ),
		),
		'tif_theme_alignment' => array (
		  'tif_header_alignment' =>
		  array (
		    'justify_content' => 'center',
		  ),
		  'tif_menu_primary_alignment' =>
		  array (
		    'justify_content' => 'center',
		  ),
		  'tif_top_bar_alignment' =>
		  array (
		    'justify_content' => 'center',
		  ),
		),
		// SETTINGS
		'tif_theme_init' => array (
		  'tif_layout' =>
		  array (
		    'home' => 'no_sidebar',
		    'post' => 'no_sidebar',
		    'page' => 'no_sidebar',
		    'archive' => 'no_sidebar',
		    'search' => 'no_sidebar',
		    'attachment' => 'no_sidebar',
		    'error404' => 'no_sidebar',
		  ),
		  'tif_social_url' =>
		  array (
		    0 => 'twitter:https%3A%2F%2Ftwitter.com%2F%23',
		    1 => 'facebook:https%3A%2F%2Ffacebook.com%2F%23',
		    2 => 'linkedin:https%3A%2F%2Flinkedin.com%2F%23',
		    3 => 'youtube:https%3A%2F%2Fyoutube.com%2F%23',
		    4 => 'diaspora:https%3A%2F%2Fdiaspora.com%2F%23',
		    5 => 'mastodon:https%3A%2F%2Fmastodon.com%2F%23',
		  ),
		),
		'tif_theme_header' => array (
		  'tif_header_after_order' =>
		  array (
		    0 => 'breadcrumb:1',
		    1 => 'title_bar:0',
		    2 => 'widget_area:1',
		  ),
		),
		'tif_theme_breadcrumb' => array (
		  'tif_breadcrumb_enabled' =>
		  array (
		    0 => '',
		  ),
		),
		'tif_theme_loop' => array (
		  'tif_loop_settings' =>
		  array (
		    'layout' =>
		    array (
		      0 => 'row',
		      1 => 1,
		      2 => 'medium',
		      3 => '',
		      4 => '',
		    ),
		  ),
		  'tif_loop_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_read_more:0',
		    5 => 'post_tags:0',
		  ),
		  'tif_archive_loop_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_read_more:0',
		    5 => 'post_tags:0',
		  ),
		  'tif_search_loop_entry_order' =>
		  array (
		    0 => 'post_thumbnail:0',
		    1 => 'post_title:1',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_read_more:0',
		    5 => 'post_tags:0',
		  ),
		  'tif_archive_loop_settings' =>
		  array (
		    'layout' =>
		    array (
		      0 => 'row',
		      1 => 3,
		      2 => 'medium',
		      3 => '',
		      4 => '',
		    ),
		  ),
		  'tif_search_loop_settings' =>
		  array (
		    'layout' =>
		    array (
		      0 => 'column',
		      1 => 1,
		      2 => 'medium',
		      3 => '',
		      4 => '',
		    ),
		  ),
		  'tif_home_loop_meta_order' =>
		  array (
		    0 => 'meta_author:0',
		    1 => 'meta_published:0',
		    2 => 'meta_category:1',
		    3 => 'meta_comments:1',
		    4 => 'meta_modified:0',
		  ),
		  'tif_archive_loop_meta_order' =>
		  array (
		    0 => 'meta_author:0',
		    1 => 'meta_published:0',
		    2 => 'meta_category:1',
		    3 => 'meta_comments:1',
		    4 => 'meta_modified:0',
		  ),
		  'tif_search_loop_meta_order' =>
		  array (
		    0 => 'meta_author:0',
		    1 => 'meta_published:0',
		    2 => 'meta_category:1',
		    3 => 'meta_comments:0',
		    4 => 'meta_modified:0',
		  ),
		),
		// POST COMPONENTS
		'tif_theme_post_cover' => array (
		  'tif_post_cover_settings' =>
		  array (
		    'entry_order' =>
		    array (
		      0 => 'breadcrumb:0',
		      1 => 'title_bar:0',
		      2 => 'post_title:1',
		      3 => 'post_thumbnail:1',
		      4 => 'post_cover_meta:1',
		      5 => 'post_excerpt:1',
		    ),
		    'meta_order' =>
		    array (
		      0 => 'meta_avatar:0',
		      1 => 'meta_author:1',
		      2 => 'meta_published:1',
		      3 => 'meta_modified:1',
		      4 => 'meta_category:1',
		      5 => 'meta_comments:1',
		    ),
		  ),
		),
		'tif_theme_post_related' => array (
		  'tif_post_related_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_read_more:1',
		    5 => 'post_tags:1',
		  ),
		  'tif_post_related_meta_order' =>
		  array (
		    0 => 'meta_author:0',
		    1 => 'meta_published:0',
		    2 => 'meta_category:1',
		    3 => 'meta_comments:0',
		  ),
		),
		'tif_theme_post_share' => array (
		  'tif_post_share_order' =>
		  array (
		    0 => 'facebook:1',
		    1 => 'twitter:1',
		    2 => 'linkedin:1',
		    3 => 'viadeo:1',
		    4 => 'pinterest:1',
		    5 => 'envelope-o:1',
		  ),
		),
		// THEME COMPONENTS
		// RESTRICTED
		// TEMPLATE
		'tif_theme_contact' => array (
		  'tif_contact_order' =>
		  array (
		    0 => 'post_title:0',
		    1 => 'post_excerpt:1',
		    2 => 'contact_form:1',
		    3 => 'post_content:1',
		    4 => 'contact_map:0',
		  ),
		  'tif_contact_sidebar_order' =>
		  array (
		    0 => 'post_excerpt:0',
		    1 => 'contact_form:0',
		    2 => 'contact_map:1',
		  ),
		  'tif_contact_form' =>
		  array (
		    'form_order' =>
		    array (
		      0 => 'name:0',
		      1 => 'email:1',
		      2 => 'matter:1',
		      3 => 'message:1',
		      4 => 'copy:1',
		    ),
		  ),
		),

	);

}

function tif_template_magazine_setup_data() {

	return array(

		// GLOBALS
		'tif_theme_order' => array (
		  'tif_post_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_thumbnail:1',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_pagination:1',
		    5 => 'post_tags:1',
		    6 => 'post_share:1',
		    7 => 'post_author:0',
		    8 => 'post_related:1',
		    9 => 'post_navigation:1',
		    10 => 'post_comments:0',
		    11 => 'site_content_bottom:0',
		  ),
		  'tif_page_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_thumbnail:1',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_pagination:1',
		    5 => 'post_tags:1',
		    6 => 'post_share:1',
		    7 => 'post_author:0',
		    8 => 'post_comments:0',
		    9 => 'site_content_bottom:0',
		  ),
		  'tif_attachment_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_attachment:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_share:1',
		    5 => 'post_author:0',
		    6 => 'post_comments:0',
		    7 => 'site_content_bottom:0',
		  ),
		  'tif_attachment_meta_order' =>
		  array (
		    0 => 'meta_avatar:0',
		    1 => 'meta_author:1',
		    2 => 'meta_published:1',
		    3 => 'meta_modified:0',
		    4 => 'meta_comments:0',
		  ),
		),
		// SETTINGS
		'tif_theme_header' => array (
		  'tif_header_after_order' =>
		  array (
		    0 => 'breadcrumb:1',
		    1 => 'title_bar:1',
		    2 => 'widget_area:1',
		  ),
		),
		'tif_theme_breadcrumb' => array (
		  'tif_breadcrumb_enabled' =>
		  array (
		    0 => 'home',
		    1 => 'static',
		    2 => 'blog',
		    3 => 'post',
		    4 => 'page',
		    5 => 'category',
		    6 => 'tag',
		    7 => 'search',
		    8 => 'author',
		    9 => 'date',
		    10 => 'attachment',
		    11 => 'error404',
		  ),
		  'tif_breadcrumb_colors' =>
		  array (
		    'bgcolor' =>
		    array (
		      0 => 'dark_accent',
		      1 => 'normal',
		      2 => 1,
		    ),
		  ),
		),
		'tif_theme_title_bar' => array (
		  'tif_title_bar_enabled' =>
		  array (
		    0 => 'home',
		    1 => 'static',
		  ),
		),
		// POST COMPONENTS
		'tif_theme_post_cover' => array (
		  'tif_post_cover_settings' =>
		  array (
		    'cover_enabled' =>
		    array (
		      0 => 'static',
		      1 => 'post',
		      2 => 'page',
		      3 => 'category',
		      4 => 'tag',
		      5 => 'search',
		      6 => 'author',
		      7 => 'date',
		    ),
		  ),
		  'tif_post_cover_box' =>
		  array (
		    'box_shadow' =>
		    array (
		      0 => -0.1,
		      1 => 0.9,
		      2 => 5.0,
		      3 => 0.0,
		      4 => 'black',
		      5 => 0.25,
		      6 => '',
		    ),
		  ),
		),
		'tif_theme_post_share' => array (
		  'tif_post_share_alignment' =>
		  array (
		    'flex_basis' =>
		    array (
		      0 => 100.0,
		      1 => '%',
		    ),
		    'gap' =>
		    array (
		      0 => 'gap_none',
		    ),
		  ),
		  'tif_post_share_box' =>
		  array (
		    'border_width' =>
		    array (
		      0 => '0',
		      1 => '0',
		      2 => '0',
		      3 => '0',
		    ),
		    'border_radius' =>
		    array (
		      0 => 'radius_none',
		    ),
		  ),
		  'tif_post_share_colors' =>
		  array (
		    'bgcolor' => 'bg_network',
		  ),
		  'tif_post_share_size' => '',
		),
		// THEME COMPONENTS
		// RESTRICTED
		// TEMPLATE

	);

}

function tif_template_blog_setup_data() {

	return array(

		// GLOBALS
		'tif_theme_order' => array (
		  'tif_post_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_thumbnail:0',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_pagination:1',
		    5 => 'post_tags:1',
		    6 => 'post_share:1',
		    7 => 'post_author:0',
		    8 => 'post_related:1',
		    9 => 'post_navigation:1',
		    10 => 'post_comments:0',
		    11 => 'site_content_bottom:0',
		  ),
		  'tif_page_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_thumbnail:0',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_pagination:1',
		    5 => 'post_tags:1',
		    6 => 'post_share:1',
		    7 => 'post_author:0',
		    8 => 'post_comments:0',
		    9 => 'site_content_bottom:0',
		  ),
		  'tif_attachment_entry_order' =>
		  array (
		    0 => 'post_title:1',
		    1 => 'post_attachment:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_share:1',
		    5 => 'post_author:0',
		    6 => 'post_comments:0',
		    7 => 'site_content_bottom:0',
		  ),
		  'tif_attachment_meta_order' =>
		  array (
		    0 => 'meta_avatar:0',
		    1 => 'meta_author:1',
		    2 => 'meta_published:1',
		    3 => 'meta_modified:0',
		    4 => 'meta_comments:0',
		  ),
		),
		// SETTINGS
		'tif_theme_init' => array (
		  'tif_site_content_alignment' =>
		  array (
		    'gap' =>
		    array (
		      0 => 'tif_length_large',
		    ),
		  ),
		),
		'tif_theme_header' => array (
		  'tif_header_after_order' =>
		  array (
		    0 => 'breadcrumb:1',
		    1 => 'title_bar:0',
		    2 => 'widget_area:1',
		  ),
		),
		'tif_theme_breadcrumb' => array (
		  'tif_breadcrumb_colors' =>
		  array (
		    'bgcolor' =>
		    array (
		      0 => 'dark_accent',
		      1 => 'normal',
		      2 => 1,
		    ),
		  ),
		),
		'tif_theme_title_bar' => array (
		  'tif_title_bar_enabled' =>
		  array (
		    0 => '',
		  ),
		),
		'tif_theme_loop' => array (
		  'tif_loop_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_read_more:0',
		    5 => 'post_tags:0',
		  ),
		  'tif_archive_loop_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_read_more:1',
		    5 => 'post_tags:1',
		  ),
		  'tif_search_loop_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:0',
		    3 => 'post_content:1',
		    4 => 'post_read_more:0',
		    5 => 'post_tags:0',
		  ),
		),
		// POST COMPONENTS
		'tif_theme_post_cover' => array (
		  'tif_post_cover_settings' =>
		  array (
		    'cover_enabled' =>
		    array (
		      0 => 'static',
		      1 => 'post',
		      2 => 'page',
		      3 => 'category',
		      4 => 'tag',
		      5 => 'search',
		      6 => 'author',
		      7 => 'date',
		    ),
		    'layout' =>
		    array (
		      0 => 'column',
		      1 => '',
		      2 => '',
		    ),
		    'entry_order' =>
		    array (
		      0 => 'breadcrumb:0',
		      1 => 'title_bar:0',
		      2 => 'post_title:1',
		      3 => 'post_thumbnail:0',
		      4 => 'post_cover_meta:1',
		      5 => 'post_excerpt:1',
		    ),
		  ),
		),
		'tif_theme_post_related' => array (
		  'tif_post_related_settings' =>
		  array (
		    'layout' =>
		    array (
		      0 => 'row',
		      1 => 1,
		      2 => 'medium',
		      3 => '',
		      4 => '',
		    ),
		  ),
		  'tif_post_related_entry_order' =>
		  array (
		    0 => 'post_thumbnail:1',
		    1 => 'post_title:1',
		    2 => 'post_meta:1',
		    3 => 'post_content:1',
		    4 => 'post_read_more:0',
		    5 => 'post_tags:0',
		  ),
		),
		'tif_theme_post_share' => array (
		  'tif_post_share_alignment' =>
		  array (
		    'flex_basis' =>
		    array (
		      0 => 0.0,
		      1 => '%',
		    ),
		    'gap' =>
		    array (
		      0 => 'gap_large',
		    ),
		  ),
		  'tif_post_share_box' =>
		  array (
		    'border_width' =>
		    array (
		      0 => '0',
		      1 => '0',
		      2 => '0',
		      3 => '0',
		    ),
		    'border_radius' =>
		    array (
		      0 => 'radius_circle',
		    ),
		  ),
		  'tif_post_share_colors' =>
		  array (
		    'bgcolor' => 'bg_network',
		  ),
		  'tif_post_share_size' => '',
		  'tif_post_share_container_alignment' =>
		  array (
		    'align_items' => 'center',
		  ),
		),
		// THEME COMPONENTS
		// RESTRICTED
		// TEMPLATE

	);

}
