<?php
/**
 * Plugin Name: Tif - Tweaks
 * Plugin URI: https://themesinfrance.fr/plugins/tif-tweaks/
 * Description: This plugin gathers some lines of code frequently used to secure its website or improve its performance. It also proposes some optimizations specific to the Canopée theme.
 * Version: 0.1
 * Author: Frédéric Caffin
 * Author URI: https://themesinfrance.fr
 * Text Domain: tif-tweaks
 * Domain Path: /inc/lang
 *
 * License: GPLv2 or later

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ( at your option ) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'TIF_TWEAKS_VERSION', '1.0' );
define( 'TIF_TWEAKS_DIR', plugin_dir_path( __FILE__ ) );
define( 'TIF_TWEAKS_URL', plugins_url( '/', __FILE__ ) );
define( 'TIF_TWEAKS_ADMIN_IMAGES_URL', plugins_url( '/assets/img/admin', __FILE__ ) ) ;

/**
 * register_uninstall_hook( $file, $callback );
 *
 * @link https://codex.wordpress.org/Function_Reference/register_uninstall_hook
 */
register_uninstall_hook( __FILE__, 'tif_tweaks_uninstall' );
function tif_tweaks_uninstall() {

	delete_option( 'tif_plugin_tweaks' );

}

if ( ! defined( 'TIF_COMPONENTS_DIR' ) )
	define( 'TIF_COMPONENTS_DIR', ( get_theme_mod( 'tif_components_version' ) ? get_template_directory() . '/tif/' : plugin_dir_path( __FILE__ ) . 'tif/' ) );

require_once TIF_COMPONENTS_DIR . 'init.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-minify.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-assets.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-posts-lists.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-thumbnail.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-mail.php';


// Tweaks Setup data
require_once 'inc/tif-setup-data.php';
require_once 'inc/tif-templates/tif-simple-setup-data.php';

// Tweaks functions
require_once 'inc/tif-functions.php';

if( is_admin() ) {

	// Back
	require_once 'inc/admin/tif-settings-form.php';
	require_once 'inc/admin/tif-register.php';

}

/**
 * Tweaks Class Init
**/
class Tif_Tweaks_Init {

	/**
	 * Setup class.
	**/
	function __construct() {

		// Tweaks Back scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'tif_tweaks_admin_scripts' ) );

		// Tweaks Front scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'tif_tweaks_public_scripts' ), 110 );

		// Tweaks optimisations scripts
		add_action( 'plugins_loaded', array( $this, 'tif_tweaks_optimizations' ) );

		// Tweaks Translation
		add_action( 'init', array( $this, 'tif_tweaks_translation' ) );
		// add_action( 'admin_init', array( $this, 'tif_tweaks_polylang' ) );

		// ... Tweaks Assets Generation .....................................
		// Add Tweaks CSS to generated main CSS if configured
		add_filter( 'tif_main_css', array( $this, 'tif_get_compiled_tweaks_css' ), 30 );

		// Add Tweaks JS to generated main JS if configured
		// add_filter( 'tif_main_js', array( $this, 'tif_get_tweaks_js' ), 30 );

		// Build standalone Tweaks CSS
		add_action( 'after_setup_theme', array( $this, 'tif_generate_css_from_fo' ), 20 );
		add_action( 'customize_save_after', array( $this, 'tif_create_tweaks_css' ), 20, false );

		// Inline Tweaks CSS if required
		add_action( 'wp_head', array( $this, 'tif_tweaks_inline_css' ), 20 );

		if( is_admin() ) {

			// Display admin notices
			add_action( 'admin_notices', array( $this, 'tif_tweaks_admin_notice' ), 10 );

			// Tweaks Menu
			add_action( 'admin_menu', array( $this, 'tif_tweaks_menu' ), 10 );

			// Tweaks Capability
			add_filter( 'option_page_capability_tif-tweaks', array( $this, 'tif_tweaks_option_page_capability' ) );

			add_action( 'init', array( $this, 'tif_tweaks_deregister_theme_customizer' ) );

			if ( ! is_customize_preview() )
				// Tweaks Register
				add_action( 'admin_init', array( $this, 'tif_tweaks_register' ) );

		}

	}

	/**
	 * Display admin notices
	 */
	public function tif_tweaks_admin_notice() {

		$settings_errors = get_settings_errors( 'tweaks_settings_error', false );

		// DEBUG:
		// tif_print_r($settings_errors);

		// Get the current screen
		$screen = get_current_screen();

		// Return if not plugin settings page
		if ( $screen->id !== 'toplevel_page_tif-tweaks-options') return;

		// Checks if settings updated
		if ( isset( $settings_errors[0]['type'] ) && $settings_errors[0]['type'] == 'updated' ) {

			// $this->tif_generate_css_from_bo();
			echo '<div class="notice notice-success"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		} elseif ( isset ( $settings_errors[0]['type'] )) {

			echo '<div class="notice notice-warning"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		}

	}

	/**
	 * Get Tweaks generated assets path
	 */
	static function tif_plugin_assets_dir() {

		$tif_dir = array();
		$dirs = wp_upload_dir();
		$tif_dir['basedir'] = $dirs['basedir'] . '/plugins/tif-tweaks';
		$tif_dir['baseurl'] = $dirs['baseurl'] . '/plugins/tif-tweaks';

		/**
		 * @see https://developer.wordpress.org/reference/functions/wp_mkdir_p/
		 */

		$mkdir = array(
			'',
			'/assets',
			'/assets/css',
			// '/assets/css/blocks',
			'/assets/js',
			// '/assets/img',
			// '/assets/fonts'
		);

		foreach ( $mkdir as $key ) {

			if ( ! is_dir( $tif_dir['basedir'] . $key ) )
				wp_mkdir_p( $tif_dir['basedir'] . $key );

		}

		return $tif_dir;

	}

	/**
	 * Tweaks admin scripts
	 */
	public function tif_tweaks_admin_scripts( $hook ) {

		if ( 'toplevel_page_tif-tweaks-options' != $hook )
			return;

		// Tweaks Back CSS
		// wp_register_style( 'tif-tweaks-admin', TIF_TWEAKS_URL . 'assets/css/admin/style' . tif_get_min_suffix() . '.css' );
		// wp_enqueue_style( 'tif-tweaks-admin' );

		// Tweaks Back JS
		wp_register_script( 'tif-tweaks-admin', TIF_TWEAKS_URL . 'assets/js/admin/script' . tif_get_min_suffix() . '.js', array( 'jquery' ), time(), true );
		wp_enqueue_script( 'tif-tweaks-admin' );
		wp_localize_script(
			'tif-tweaks-admin',
			'tif_tweaks_object',
			array(
				'userid' => get_current_user_id(),
				'restriction_message' => addslashes( esc_html__( 'You have restricted access to the Setup tab to administrator(s) other than yourself. Do you really want to deny yourself access to this tab?', 'tif-tweaks' ) ),
				'reset_message' => addslashes( esc_html__( 'This will reset all theme settings and restore the default options.', 'tif-tweaks' ) ),
			),
			false
		);


	}

	/**
	 * Tweaks front scripts
	 */
	public function tif_tweaks_public_scripts() {

		$tif_generated = array( 'css' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		$deps          = get_theme_mod( 'tif_components_version' ) ? array( 'tif-main' ) : false;

		if ( ! $deps || ( $deps && ! in_array( 'css', $tif_generated ) && ! is_customize_preview() ) ) {

			// Get Tweaks CSS dir
			$tif_dir	 = $this->tif_plugin_assets_dir();

			$filetocheck = $tif_dir['basedir'] . '/assets/css/style.css';
			$version	 = file_exists( $filetocheck ) ? date ( 'YdmHis', filemtime( $filetocheck ) ) : time();

			// Tweaks Front CSS if not added to TIF Main CSS
			wp_register_style( 'tif-tweaks', $tif_dir['baseurl'] . '/assets/css/style' . tif_get_min_suffix() . '.css', $deps, $version );
			wp_enqueue_style( 'tif-tweaks' );

		}

		// if ( ! $deps || ( $deps && ! in_array( 'js', $tif_generated ) ) ) {
		//
		// 	// Tweaks Front JS if not added to TIF Main JS
		// 	wp_register_script( 'tif-tweaks', TIF_TWEAKS_URL . 'assets/js/script' . tif_get_min_suffix() . '.js', array( 'jquery' ), time(), true );
		// 	wp_enqueue_script( 'tif-tweaks' );
		//
		// }

	}

	/**
	 * Tweaks capability
	 */
	static function tif_tweaks_menu_title() {

		$menu_title = tif_get_option( 'plugin_tweaks', 'tif_init,menu_title', 'string' );
		$menu_title = ( null != $menu_title ) ? $menu_title : __( 'Tweaks', 'tif-tweaks' );
		return $menu_title;

	}

	/**
	 * Tweaks capability
	 */
	public function tif_tweaks_option_page_capability( $capability ) {

		if ( current_user_can( 'edit_posts' ) )
			return 'edit_posts';

	}

	/**
	 * Add Tweaks admin menu
	 */
	public function tif_tweaks_menu() {

		// @link https://www.base64-image.de/
		// @link http://b64.io/

		$icon_png = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAK/SURBVDiN1ZNPiJRlHMc/3+d9x11xRT1V27qQYUtBBgZLlyCQLv2xvAzurs3A7gwDBp6CIkIn0IjoEB6Eydl9W3V3aLpEgl6iKYLwJKFmhzT8s7V2KS3d1eZ9np+H3Zkmc7x0qe/pfV++v8/z/f4eXvivSwDlctn1Pzz0lC30fVsqvbgAUDlUH4wsfcEkRSmfmeFDhm0OvEXheGFs7CJAkiS9XpknJ/Jj30gyAVSnZz8w7CqKPv5p/X0/PHh5/nUZbwG9ywf/AXhgLYDBIth7cfjzXctkNpjXy4YeKuRHi44lx3ahOQgPDFyaPyFjbwcMYHULtlxrpdAe73pOpp4NmM4ZjJmZ4iWHzoEdlHVdzRUgBQbu+P6owx01AeK0JHNLAW0n4se7gLyJQ02lTzgfbwYqQPMuvgsy5duXAlBuNOLBSz8/Y2hTEHKBC5bGjUIh+2vn5IGZmXWZ1G1x2KBh3pzOrumJv8hms75rv38jdb5Up2eGgTKmW4hfnNe+8fGRy52eJKnfn7r0TYmNACa9U3xl5Ot/AMuNRjxwcf7YCpduy+VyN6aOHHkseL1vohp7dzKNmnIh3myyCUNvFPOjp+r1+srfF5ufLlxbt3XXruduAbgWsH/uyrBkX+ZyuRsA4zt2nPU3r7/kzD3uXXhVFu002DQ32L+1mB89BZDNZhclHe9b89vTLU7cjuptCNyZznqlUqkJvH2vnQXxnUyPAJ//LaEkdZ26h6JgBtbmtBM62XmD4TsHqtO1cWGrDDNQcyI38qH01y/g5Yac6Xyb03q4fnXtCWBLvV5fATA5WeuvfjRbg7A6BJ224M4A0eTh2idTU7X1sHSRsvB8enPVV+2mnWkOHp59VsFeA3cNQtMU7S3mtn/f6UmSmY3eabeg10QfcKCQGz3adSeVSiWTJElvV8Oy9u8/1tNq8//Sbb8YGVFC1A+JAAAAAElFTkSuQmCC';
		$icon_svg = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiICAgd2lkdGg9IjIwIiAgIGhlaWdodD0iMjAiICAgdmlld0JveD0iMCAwIDIwIDIwIiAgIGlkPSJzdmczNTgyIiAgIHZlcnNpb249IjEuMSIgICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkxIHIxMzcyNSIgICBzb2RpcG9kaTpkb2NuYW1lPSJpY29uLnN2ZyI+ICA8ZGVmcyAgICAgaWQ9ImRlZnMzNTg0IiAvPiAgPHNvZGlwb2RpOm5hbWVkdmlldyAgICAgaWQ9ImJhc2UiICAgICBwYWdlY29sb3I9IiNmZmZmZmYiICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIgICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIgICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIiAgICAgaW5rc2NhcGU6em9vbT0iMTUuODM5MTkyIiAgICAgaW5rc2NhcGU6Y3g9IjkuNjQwNTkwNSIgICAgIGlua3NjYXBlOmN5PSIxNC40MjgzNzIiICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0icHgiICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJsYXllcjEiICAgICBzaG93Z3JpZD0iZmFsc2UiICAgICB1bml0cz0icHgiICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE2MDAiICAgICBpbmtzY2FwZTp3aW5kb3ctaGVpZ2h0PSI4NzUiICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMCIgICAgIGlua3NjYXBlOndpbmRvdy15PSIyNSIgICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiIC8+ICA8bWV0YWRhdGEgICAgIGlkPSJtZXRhZGF0YTM1ODciPiAgICA8cmRmOlJERj4gICAgICA8Y2M6V29yayAgICAgICAgIHJkZjphYm91dD0iIj4gICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PiAgICAgICAgPGRjOnR5cGUgICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+ICAgICAgICA8ZGM6dGl0bGUgLz4gICAgICA8L2NjOldvcms+ICAgIDwvcmRmOlJERj4gIDwvbWV0YWRhdGE+ICA8ZyAgICAgaW5rc2NhcGU6bGFiZWw9IkNhbHF1ZSAxIiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIgICAgIGlkPSJsYXllcjEiICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMyLjM2MjIpIj4gICAgPHBhdGggICAgICAgc3R5bGU9ImRpc3BsYXk6aW5saW5lO2ZpbGw6bm9uZTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzljYTFhNjtzdHJva2Utd2lkdGg6MC42ODc0MzcwNjtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIiAgICAgICBkPSJNIDQuOTYyODkwNiA5LjM1NzQyMTkgQyA0LjcwMzA0ODggOS4zNzI3ODc3IDQuNDU5MTkxNyA5LjQ0Mzc1IDQuMjQwMjM0NCA5LjU4NTkzNzUgQyAyLjQ4ODQyMSAxMC43MjMzMzggMi40ODg0MjEgMTUuMzcwMzEyIDQuMjQwMjM0NCAxNi41MDc4MTIgQyA1LjU0NjY4IDE3LjM1NjE5OCA3Ljc1MTE3MzEgMTUuNjczNzU3IDguNzg5MDYyNSAxNC43NjE3MTkgQyA4Ljk1NjM4MjcgMTQuODUxMjk0IDkuMTM4MDE3NCAxNC45MTQwNjIgOS4zNDE3OTY5IDE0LjkxNDA2MiBMIDEwLjY5OTIxOSAxNC45MTQwNjIgQyAxMC45MDI5OTggMTQuOTE0MDYyIDExLjA4NDYzMyAxNC44NTEyOTQgMTEuMjUxOTUzIDE0Ljc2MTcxOSBDIDEyLjI4OTg0MiAxNS42NzM3NTcgMTQuNDk2Mjg5IDE3LjM1NjE5OCAxNS44MDI3MzQgMTYuNTA3ODEyIEMgMTcuNTU0NTQ3IDE1LjM3MDMxMiAxNy41NTQ1NDcgMTAuNzIzMzM4IDE1LjgwMjczNCA5LjU4NTkzNzUgQyAxNC40OTY1NzggOC43Mzc4MTQ5IDEyLjI5MjE2MiAxMC40MjE3MjEgMTEuMjUzOTA2IDExLjMzMzk4NCBDIDExLjA4NjE5IDExLjI0Mzg1MSAxMC45MDM2NzcgMTEuMTc5Njg4IDEwLjY5OTIxOSAxMS4xNzk2ODggTCA5LjM0MTc5NjkgMTEuMTc5Njg4IEMgOS4xMzczMzgxIDExLjE3OTY4OCA4Ljk1NDgyNTUgMTEuMjQzODUxIDguNzg3MTA5NCAxMS4zMzM5ODQgQyA3LjkyMjg4NjMgMTAuNTc0NjYyIDYuMjUyOTU3OSA5LjI4MTEzMzQgNC45NjI4OTA2IDkuMzU3NDIxOSB6ICIgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwxMDMyLjM2MjIpIiAgICAgICBpZD0icGF0aDM3NDMiIC8+ICAgIDxnICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDkuNzAwMTAwN2UtNCwwLDAsLTAuMDAxMDUwOTYsMC41MTMyMzI5NiwxMDQwLjk1ODMpIiAgICAgICBpZD0iZzM3NTEiICAgICAgIHN0eWxlPSJkaXNwbGF5OmlubGluZTtmaWxsOiM5Y2ExYTY7c3Ryb2tlOm5vbmU7ZmlsbC1vcGFjaXR5OjEiPiAgICAgIDxwYXRoICAgICAgICAgZD0iTSA3MzUwLDU3NzkgQyA3MDA1LDU3NDEgNjYyOCw1NjQxIDYyNzcsNTQ5MyA1ODUyLDUzMTUgNTQ0NSw1MDc5IDQ3NDUsNDYwNSA0NTY5LDQ0ODUgNDMzMyw0MzI3IDQyMjAsNDI1MiAzNTE5LDM3ODggMzAyNSwzNTYzIDI1ODUsMzUwNSBjIC0yMjksLTMwIC0zMzgsLTE2IC00ODUsNjEgLTE4NCw5NiAtMzE1LDI2NSAtMzY1LDQ2OCAtMzYsMTQ5IC0zNiwzNzYgMCw1MTcgNTUsMjE0IDIwMSwzOTggMzk0LDQ5OCAxMTUsNTkgMTk5LDgyIDMyNyw4OCAyMjcsMTEgNDExLC02MyA1NzMsLTIzMSA1MiwtNTQgNjksLTY2IDk1LC02NiA0OSwwIDgwLDM4IDczLDg4IC0xMiw5MSAtMTEyLDI1NiAtMjMxLDM4MCAtMTMzLDEzOSAtMjg1LDIyNiAtNDc2LDI3NSAtMTE0LDI5IC0zNzIsMzEgLTUwMCw0IEMgMTUwMyw1NDg1IDExMzMsNTIzNiA4NjAsNDgyNyA0NTYsNDIyMiA0MTMsMzQ1OSA3NDYsMjc5MSBjIDI1NSwtNTEzIDYxNiwtODU3IDEyMDYsLTExNTEgNTgxLC0yODkgMTI4NSwtNDgwIDIwNjgsLTU1OSAxMTk2LC0xMjIgMjQzMiw0NSAzNTgwLDQ4NCA3NDEsMjg0IDE1MDUsNzIzIDIxMjQsMTIyMiBsIDc4LDYzIDM3LC0zMCBjIDE3OSwtMTQ5IDU4MCwtNDM1IDgxNiwtNTgyIDE2ODgsLTEwNTEgMzczMiwtMTQzNCA1NjI1LC0xMDUzIDEwNDEsMjEwIDE4NjIsNjIxIDIyODMsMTE0NSAzMzcsNDE5IDUxNyw5MDEgNTE3LDEzODcgMCw1MjQgLTIwMiwxMDIzIC01NjAsMTM4MyAtMjI3LDIyOSAtNDQ1LDM2MCAtNzQxLDQ0NSAtNDY0LDEzNCAtODQ2LDU4IC0xMTMwLC0yMjQgLTExNCwtMTE0IC0yNDgsLTMzMyAtMjQ5LC00MDkgMCwtMzMgNDMsLTcyIDc5LC03MiAyNSwwIDQ2LDE1IDEwOSw3OCAxNDAsMTQxIDI5MywyMTAgNDgyLDIyMCA0MjAsMjAgNzY0LC0yNzUgODIxLC03MDMgMTUsLTExMyA2LC0yODIgLTIxLC0zOTUgLTYzLC0yNjQgLTI2MiwtNDY4IC01MjEsLTUzNSAtMTk3LC01MSAtNjEyLDMwIC05OTQsMTk0IC0zNDksMTUwIC02NzUsMzQ5IC0xNTQwLDkzNiAtNzg2LDUzMyAtMTI1NCw3ODggLTE3NzAsOTY1IC0zOTUsMTM0IC03MTcsMTkwIC0xMTExLDE5MCAtNTI4LDAgLTEwMjksLTk2IC0xNDM2LC0yNzUgLTIxOCwtOTYgLTQzMywtMjM5IC02MTEsLTQwNSBsIC04NywtODIgLTkzLDg4IGMgLTM2NCwzNDkgLTg0OCw1NTIgLTE1MzAsNjQ1IC0xNzQsMjMgLTY3OCwzNSAtODI3LDE4IHoiICAgICAgICAgaWQ9InBhdGgzNzUzIiAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiICAgICAgICAgc3R5bGU9ImZpbGw6IzljYTFhNjtmaWxsLW9wYWNpdHk6MSIgLz4gICAgPC9nPiAgICA8ZyAgICAgICBzdHlsZT0iZGlzcGxheTppbmxpbmU7ZmlsbDpub25lO3N0cm9rZTpub25lIiAgICAgICBpZD0iZzM3NTUiICAgICAgIHRyYW5zZm9ybT0ibWF0cml4KDkuNzAwMTAwN2UtNCwwLDAsLTAuMDAxMDUwOTYsMC41MTMyMzI5NiwxMDQwLjk1ODMpIj4gICAgICA8cGF0aCAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiICAgICAgICAgaWQ9InBhdGgzNzU3IiAgICAgICAgIGQ9Ik0gNzM1MCw1Nzc5IEMgNzAwNSw1NzQxIDY2MjgsNTY0MSA2Mjc3LDU0OTMgNTg1Miw1MzE1IDU0NDUsNTA3OSA0NzQ1LDQ2MDUgNDU2OSw0NDg1IDQzMzMsNDMyNyA0MjIwLDQyNTIgMzUxOSwzNzg4IDMwMjUsMzU2MyAyNTg1LDM1MDUgYyAtMjI5LC0zMCAtMzM4LC0xNiAtNDg1LDYxIC0xODQsOTYgLTMxNSwyNjUgLTM2NSw0NjggLTM2LDE0OSAtMzYsMzc2IDAsNTE3IDU1LDIxNCAyMDEsMzk4IDM5NCw0OTggMTE1LDU5IDE5OSw4MiAzMjcsODggMjI3LDExIDQxMSwtNjMgNTczLC0yMzEgNTIsLTU0IDY5LC02NiA5NSwtNjYgNDksMCA4MCwzOCA3Myw4OCAtMTIsOTEgLTExMiwyNTYgLTIzMSwzODAgLTEzMywxMzkgLTI4NSwyMjYgLTQ3NiwyNzUgLTExNCwyOSAtMzcyLDMxIC01MDAsNCBDIDE1MDMsNTQ4NSAxMTMzLDUyMzYgODYwLDQ4MjcgNDU2LDQyMjIgNDEzLDM0NTkgNzQ2LDI3OTEgYyAyNTUsLTUxMyA2MTYsLTg1NyAxMjA2LC0xMTUxIDU4MSwtMjg5IDEyODUsLTQ4MCAyMDY4LC01NTkgMTE5NiwtMTIyIDI0MzIsNDUgMzU4MCw0ODQgNzQxLDI4NCAxNTA1LDcyMyAyMTI0LDEyMjIgbCA3OCw2MyAzNywtMzAgYyAxNzksLTE0OSA1ODAsLTQzNSA4MTYsLTU4MiAxNjg4LC0xMDUxIDM3MzIsLTE0MzQgNTYyNSwtMTA1MyAxMDQxLDIxMCAxODYyLDYyMSAyMjgzLDExNDUgMzM3LDQxOSA1MTcsOTAxIDUxNywxMzg3IDAsNTI0IC0yMDIsMTAyMyAtNTYwLDEzODMgLTIyNywyMjkgLTQ0NSwzNjAgLTc0MSw0NDUgLTQ2NCwxMzQgLTg0Niw1OCAtMTEzMCwtMjI0IC0xMTQsLTExNCAtMjQ4LC0zMzMgLTI0OSwtNDA5IDAsLTMzIDQzLC03MiA3OSwtNzIgMjUsMCA0NiwxNSAxMDksNzggMTQwLDE0MSAyOTMsMjEwIDQ4MiwyMjAgNDIwLDIwIDc2NCwtMjc1IDgyMSwtNzAzIDE1LC0xMTMgNiwtMjgyIC0yMSwtMzk1IC02MywtMjY0IC0yNjIsLTQ2OCAtNTIxLC01MzUgLTE5NywtNTEgLTYxMiwzMCAtOTk0LDE5NCAtMzQ5LDE1MCAtNjc1LDM0OSAtMTU0MCw5MzYgLTc4Niw1MzMgLTEyNTQsNzg4IC0xNzcwLDk2NSAtMzk1LDEzNCAtNzE3LDE5MCAtMTExMSwxOTAgLTUyOCwwIC0xMDI5LC05NiAtMTQzNiwtMjc1IC0yMTgsLTk2IC00MzMsLTIzOSAtNjExLC00MDUgbCAtODcsLTgyIC05Myw4OCBjIC0zNjQsMzQ5IC04NDgsNTUyIC0xNTMwLDY0NSAtMTc0LDIzIC02NzgsMzUgLTgyNywxOCB6IiAgICAgICAgIHN0eWxlPSJmaWxsOm5vbmUiIC8+ICAgIDwvZz4gIDwvZz48L3N2Zz4=';

		// @link https://developer.wordpress.org/reference/functions/add_menu_page/
		// @link https://codex.wordpress.org/Administration_Menus

		global $tif_tweaks_translation;
		$tif_tweaks_translation = esc_html__( 'Tweaks', 'tif-tweaks' );

		add_menu_page(
			$this->tif_tweaks_menu_title(),					// $page_title - Required - Text in browser title bar
			$this->tif_tweaks_menu_title(),					// $menu_title - Required - Text to be displayed in the menu.
			'manage_options', 								// $capability - Required
			'tif-tweaks-options', 							// $menu_slug - Required
			'tif_theme_options_page', 						// $function - Optional
			$icon_svg 										// $icon_url - Optional
			// plugins_url( 'myplugin/images/icon.png' ),	// $icon_url - Optional
			// 'images/icon.png',							// $icon_url - Optional
			// 'dashicons-chart-pie',						// $icon_url - Optional
			// 50											// $position - Optional
		);

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_tweaks', 'tif_init,capabilities', 'multicheck' ) ) ;

	}

	/**
	 * [tif_tweaks_deregister_theme_customizer description]
	 * @return [type] [description]
	 * @TODO
	 */
	public function tif_tweaks_deregister_theme_customizer() {

		// GLOBALS
		// define( 'TIF_CUSTOMISER_THEME_ORDER', false );
		// define( 'TIF_CUSTOMISER_THEME_ALIGNMENT', false );
		// define( 'TIF_CUSTOMISER_THEME_FONTS', false );
		// define( 'TIF_CUSTOMISER_THEME_COLORS', false );
		// define( 'TIF_CUSTOMISER_THEME_IMAGES', false );

		// SETTINGS
		// define( 'TIF_CUSTOMISER_THEME_INIT', false );
		// define( 'TIF_CUSTOMISER_THEME_HEADER', false );
		// define( 'TIF_CUSTOMISER_THEME_NAVIGATION', false );
		// define( 'TIF_CUSTOMISER_THEME_BREADCRUMB', false );
		// define( 'TIF_CUSTOMISER_THEME_TITLE_BAR', false );
		// define( 'TIF_CUSTOMISER_THEME_LOOP', false );

		// POST COMPONENTS
		// define( 'TIF_CUSTOMISER_THEME_POST_COVER', false );
		// define( 'TIF_CUSTOMISER_THEME_POST_RELATED', false );
		// define( 'TIF_CUSTOMISER_THEME_POST_CHILD', false );
		// define( 'TIF_CUSTOMISER_THEME_POST_SHARE', false );
		// define( 'TIF_CUSTOMISER_THEME_POST_TAXONOMIES', false );

		// THEME COMPONENTS
		// define( 'TIF_CUSTOMISER_THEME_BUTTONS', false );
		// define( 'TIF_CUSTOMISER_THEME_ALERTS', false );
		// define( 'TIF_CUSTOMISER_THEME_QUOTES', false );
		// define( 'TIF_CUSTOMISER_THEME_SOCIAL', false );
		// define( 'TIF_CUSTOMISER_THEME_PAGINATION', false );

		// RESTRICTED
		// define( 'TIF_CUSTOMISER_THEME_ASSETS', false );
		// define( 'TIF_CUSTOMISER_THEME_UTILS', false );
		// define( 'TIF_CUSTOMISER_THEME_PRIVACY', false );
		// define( 'TIF_CUSTOMISER_THEME_DEBUG', false );
		// define( 'TIF_CUSTOMISER_THEME_CREDIT', false );

		// TEMPLATE
		// define( 'TIF_CUSTOMISER_THEME_WOO', false );

		// TEMPLATE
		// define( 'TIF_CUSTOMISER_THEME_CONTACT', false );

		// TEMPLATE
		// define( 'TIF_CUSTOMISER_HOMEPAGE_CONTROL', false );

		$deregister = array();
		$customizer = tif_get_option( 'plugin_tweaks', 'tif_customizer', 'array' );
		foreach ( $customizer as $key => $value ) {

			if ( is_array( $value ) )
				$deregister = array_merge( $deregister, $value );

		}

		foreach ( $deregister as $key => $value ) {
			define( 'TIF_CUSTOMISER_' . strtoupper( $value ), false );
		}

	}

	/**
	 * register_setting( $option_group, $option_name, $sanitize_callback );
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_setting
	 */
	public function tif_tweaks_register() {

		if( ! is_customize_preview() )
			register_setting( 'tif-tweaks', 'tif_plugin_tweaks', 'tif_tweaks_sanitize' );

	}

	/**
	 * Loads Tweaks translated strings
	 *
	 * @link https://developer.wordpress.org/reference/functions/load_plugin_textdomain/
	 */
	public function tif_tweaks_translation() {

		load_plugin_textdomain( 'tif-tweaks', false, basename( dirname( __FILE__ ) ) . '/inc/lang' );

	}

	public function tif_tweaks_optimizations() {

		$callback = tif_get_option( 'plugin_tweaks', 'tif_callback', 'array' );
		$callback = is_array( $callback ) ? array_filter ( $callback ) : array();

		foreach ( $callback as $key => $value) {

			// Prevent a notice
			if ( function_exists( 'tif_' . $key ) )
				call_user_func( 'tif_' . (string)$key );

		}

		$restricted_callback = tif_get_option( 'plugin_tweaks', 'tif_restricted_callback', 'array' );
		$restricted_callback = is_array( $restricted_callback ) ? array_filter ( $restricted_callback ) : array();

		foreach ( $restricted_callback as $key => $value) {

			// Prevent a notice
			if ( function_exists( 'tif_' . $key ) )
				call_user_func( 'tif_' . (string)$key );

		}

		$feeds = tif_get_option( 'plugin_tweaks', 'tif_feeds', 'array' );
		$feeds = is_array( $feeds ) ? array_filter ( $feeds ) : array();

		foreach ( $feeds as $key => $value) {

			// Prevent a notice
			if ( function_exists( 'tif_remove_' . $key . '_feed_link' ) )
				call_user_func( 'tif_remove_' . (string)$key. '_feed_link' );

		}

	}

	/**
	 * Enable Tweaks translation with Polylang
	 *
	 * @link https://polylang.pro/doc/function-reference/
	 */
	// public function tif_tweaks_polylang() {
	//
	// 	if( ! function_exists( 'pll_register_string' ) )
	// 		return;
	//
	// 	pll_register_string(
	// 		esc_html__( 'My text.', 'tif-tweaks' ),
	// 		tif_get_option( 'plugin_tweaks', 'tif_text', 'text' ),
	// 		'Tif - ' . esc_html__( 'Plugin', 'tif-tweaks' )
	// 	);
	//
	// 	pll_register_string(
	// 		esc_html__( 'My textarea.', 'tif-tweaks' ),
	// 		esc_attr( tif_get_option( 'plugin_tweaks', 'tif_textarea', 'textarea' ) ),
	// 		'Tif - ' . esc_html__( 'Plugin', 'tif-tweaks' )
	// 	);
	//
	// }

	// ... Tweaks Assets Generation .........................................

	/**
	 * Get Tweaks assets added to tif main generated
	 */
	public function tif_get_generated_assets() {

		$tif_generated = array( 'css' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		return $tif_generated;

	}

	/**
	 * Tweaks inline CSS for customize preview
	 */
	public function tif_tweaks_inline_css() {

		if ( ! is_customize_preview() )
			return;

		echo '<style type="text/css">';

		$this->tif_compile_tweaks_css( true );

		echo '</style>';

	}

	/**
	 * Get Tweaks compiled CSS
	 */
	public function tif_get_compiled_tweaks_css( $css ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(  ! class_exists ( 'Themes_In_France' )
			|| ! in_array( 'css', $tif_generated ) )
			return $css;

		$css .= $this->tif_compile_tweaks_css();

		return $css;

	}

	/**
	 * Add Tweaks CSS to tif-main.css if configured
	 */
	private function tif_compile_tweaks_css( $echo = false, $custom_css = false ) {

		$css = null;

		// Compile Plugin CSS
		if ( class_exists ( 'Themes_In_France' ) )
			$css .= tif_compile_scss(
				array(
					'origin'		=> 'plugin',
					'components'	=> array( '_style' ),
					'variables'		=> array(),
					'path'			=> TIF_TWEAKS_DIR . 'assets/scss'
				)
			);

		if ( $echo )
			echo $css;

		else
			return $css;

	}

	/**
	 * Create Plugin CSS
	 */
	public function tif_create_tweaks_css( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_tweaks', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-tweaks' ) );

		// Get plugin CSS dir
		$tif_dir	= $this->tif_plugin_assets_dir();

		// Compile plugin CSS
		$plugin_css = $this->tif_compile_tweaks_css( false, $custom_css );

		// Create CSS
		tif_create_assets(
			array (
				'content'	=> $plugin_css,
				'type'		=> 'css',
				'path'		=> $tif_dir['basedir'] . '/assets/css/',
				'name'		=> 'style'
			)
		);

	}

	/**
	 * Generate Plugin css after register_setting()
	 */
	private function tif_generate_css_from_bo() {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_tweaks', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-tweaks' ) );

		// Generate theme tif-main.css
		if( get_theme_mod( 'tif_components_version' ) ) {
			tif_create_theme_main_css( true );
			tif_create_theme_main_js( true );
		}

		// Generate plugin CSS
		$custom_css = null != get_option( 'tif_plugin_tweaks' )['tif_init']['custom_css']
					? get_option( 'tif_plugin_tweaks' )['tif_init']['custom_css']
					: true;
		$generated	= new Tif_Tweaks_Init;
		$generated->tif_create_tweaks_css( strip_tags( $custom_css ) );

	}

	/**
	 * Force Plugin CSS if requested
	 */
	public function tif_generate_css_from_fo( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$tif_dir	= $this->tif_plugin_assets_dir();

		if ( ( isset( $_POST['tif_generate_css_nonce_field'] )
			&& wp_verify_nonce( $_POST['tif_generate_css_nonce_field'], 'tif_generate_css' )
			&& current_user_can( 'administrator' ) )
			)
			$this->tif_create_tweaks_css();

		if ( ! file_exists( $tif_dir['basedir'] . '/assets/css/style.css' )
			|| $custom_css
			)
			$this->tif_create_tweaks_css( $custom_css );

	}

	/**
	 * Add Tweaks JS to tif-main.js if configured
	 */
	// public function tif_get_tweaks_js( $js ) {
	//
	// 	$tif_generated = $this->tif_get_generated_assets();
	//
	// 	if(    class_exists ( 'Themes_In_France' )
	// 		&& ! in_array( 'js', $tif_generated )
	// 		)
	// 		return $js;
	//
	// 		// Prevent a notice
	// 	$tif_js = array();
	// 	$tif_js_build = '';
	//
	// 	// Array of css files
	// 	$tif_js[] = TIF_TWEAKS_DIR . 'assets/js/script.js';
	//
	// 	// Loop the css Array
	// 	global $wp_filesystem;
	// 	foreach ( $tif_js as $js_file ) {
	// 		$tif_js_build .= $wp_filesystem->get_contents( $js_file );
	// 	}
	//
	// 	// return the generated css
	// 	$js .= $tif_js_build;
	//
	// 	return $js;
	//
	// }

}

return new Tif_Tweaks_Init();
