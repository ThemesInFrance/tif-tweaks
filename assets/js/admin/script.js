jQuery(document).ready( function( $ ){

    $( "#submit" ).click(function() {

        var retrictions = "tif_plugin_tweaks[tif_init][restrictions]";
        if ( $('input[name="' + retrictions + '"][type="hidden"]').val() != '' && ! $('input[name="' + retrictions + '"][value="' + tif_tweaks_object.userid + '"]').is(':checked')){
                var tifSelfRestricted = confirm( tif_tweaks_object.restriction_message );
                $('input[name="' + retrictions + '"][value="' + tif_tweaks_object.userid + '"]').focus();
                if (tifSelfRestricted == false){
                    return false;
                }
            }
    });

    $('#tif-plugin-tweaks-tif-reset-theme-mods').change(function() {
        if ($(this).prop('checked')) {
            var tifResetThemeMods = confirm( tif_tweaks_object.reset_message );
            if (tifResetThemeMods == false){
                $(this).prop('checked', false);
            }
        }
    });

});
