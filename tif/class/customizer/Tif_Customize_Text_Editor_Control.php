<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_text_editor_control' ) ) {

	add_action( 'customize_register', 'tif_extend_text_editor_control' );

	function tif_extend_text_editor_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Text_Editor_Control extends WP_Customize_Control {

			/**
			 * The type of control being rendered
			 */
			 public $type = 'tinymce_editor';

			/**
			 * Enqueue our scripts and styles
			 */
			public function enqueue(){
				wp_enqueue_editor();
			}

			/**
			 * Pass our TinyMCE toolbar string to JavaScript
			 */
			public function to_json() {
				parent::to_json();
				$this->json['tif_tinymce_toolbar1'] = isset ( $this->input_attrs['toolbar1'] ) ? esc_attr( $this->input_attrs['toolbar1'] ) : 'formatselect bold italic bullist numlist alignleft aligncenter alignright link';
				$this->json['tif_tinymce_toolbar2'] = isset ( $this->input_attrs['toolbar2'] ) ? esc_attr( $this->input_attrs['toolbar2'] ) : '';
				$this->json['tif_media_buttons'] = isset ( $this->input_attrs['mediaButtons'] ) && ( $this->input_attrs['mediaButtons'] === true ) ? true : false;
			}

			/**
			 * Render the control in the customizer
			 */
			public function render_content(){
			?>
				<div class="tinymce-control">
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
					<?php if( !empty( $this->description ) ) { ?>
						<span class="customize-control-description"><?php echo esc_html( $this->description ); ?></span>
					<?php } ?>
					<textarea id="<?php echo esc_attr( $this->id ); ?>" class="customize-control-tinymce-editor" <?php $this->link(); ?>><?php echo esc_html( $this->value() ); ?></textarea>
				</div>
			<?php

			}

		}

	}

}
