<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * The main Themes_In_France class
 */
class Tif_Init {

	/**
	 * Setup class.
	 *
	 * @since 1.0
	 */
	function __construct() {

		add_action( 'admin_print_styles', array( $this, 'tif_admin_enqueue_style' ), 100  );
		add_action( 'admin_enqueue_scripts', array( $this, 'tif_admin_enqueue_scripts' ), 100  );
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'tif_admin_enqueue_scripts' ), 100  );

	}

	public function tif_admin_enqueue_style() {

		$url = $this->tif_get_tif_url();

		wp_enqueue_style( 'tif-admin', $url . 'assets/css/tif-admin.css', '', '1.0.0' );

		// Color Picker style
		wp_enqueue_style( 'wp-color-picker' );

	}

	public function tif_admin_enqueue_scripts() {

		$url = $this->tif_get_tif_url();

		// Tif customizer extend controls
		wp_register_script( 'tif-customizer-extend-control', $url . 'assets/js/tif-customizer-extend-control.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'tif-customizer-extend-control' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-sortable' );

		// Color Picker scripts
		wp_enqueue_script( 'tif-color-picker',  $url . 'assets/js/tif-color-picker.js', array( 'wp-color-picker' ), false, true );

		// Media uploader
		// @see https://developer.wordpress.org/reference/functions/wp_enqueue_media/
		wp_register_script( 'tif-media-uploader', $url . 'assets/js/tif-media-uploader.js', array( 'jquery' ), time(), false );
		wp_enqueue_script( 'tif-media-uploader' );
		wp_enqueue_media();

	}

	public static function tif_get_tif_url() {

		$url = plugins_url( '/', __FILE__ );
		$plugin_url = substr( $url, 0, stripos( $url, '/tif/class/' ) );
		$url = ! class_exists ( 'Themes_In_France' ) ? $plugin_url : get_template_directory_uri() ;

		return esc_url( $url . '/tif/' );

	}

	public static function tif_get_tif_dir() {

		$path = plugin_dir_path( __FILE__ );
		$plugin_path = substr( $path, 0, stripos( $path, '/tif/class/' ) );
		$dir = ! class_exists ( 'Themes_In_France' ) ? $plugin_path : get_template_directory() ;

		return $dir . '/tif/';

	}

}

return new Tif_Init();
