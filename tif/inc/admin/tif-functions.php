<?php

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'customize_controls_enqueue_scripts', 'tif_customizer_jscolor_inline_scripts', 100 );
if ( ! function_exists('tif_customizer_jscolor_inline_scripts') ) {

	function tif_customizer_jscolor_inline_scripts() {

		$jscolor_presets ='{
			position: "bottom",
			width: 181,
			height: 181,
			backgroundColor: "#333",
			closeButton:true,
			closeText:"' . esc_html__( 'Close', 'canopee' ) . '",
			buttonColor: "rgba(240,240,240,1)",
			paletteCols: 5,
			paletteHeight:32,
			paletteWidth:32
		}';

		wp_add_inline_script( 'tif-jscolor', 'jscolor.presets.default = ' . $jscolor_presets . ';' );

	}

}

/**
 * [tif_get_theme_hex_colors description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_theme_hex_colors() {

	if ( class_exists( 'Themes_In_France' ) ) {

		$palette = tif_get_option( 'theme_colors', 'tif_palette_colors', 'array' );
		$semantic = tif_get_option( 'theme_colors', 'tif_semantic_colors', 'array' );

		$colors_palette = array(
			$palette['light'],
			$palette['light_accent'],
			$palette['primary'],
			$palette['dark_accent'],
			$palette['dark'],

			$semantic['default'],
			$semantic['info'],
			$semantic['success'],
			$semantic['warning'],
			$semantic['danger'],

			'#ffffff',
			'#000000',
		);

	} else {

		$colors_palette = array(
			"#000000",
			"#ffffff",
			"#dd3333",
			"#dd9933",
			"#eeee22",
			"#81d742",
			"#1e73be",
			"#8224e3"
		);

	}

	return array_map('tif_sanitize_hexcolor', $colors_palette);

}
