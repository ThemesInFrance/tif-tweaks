# tif-tweaks

This plugin gathers some lines of code frequently used to secure its website or improve its performance. It also proposes some optimizations specific to the Canopée theme.